package project_pdp;

import java.util.*;
import java.lang.*;
import java.io.*;

/**
 * Class for define Interval.
 * Interval is T type.
 * @author katebi
 */
public class PolicyInterval {

    class Interval<T> {

        T start;
        T end;
    }

    /**
     * function for casting object to double
     */
    public Double asDouble(Object o) {
        Double val = null;
        if (o instanceof Number) {
            val = ((Number) o).doubleValue();
        }
        return val;
    }

    /**
     * Minimum function between two objects.
     */
    public Object min(Object b, Object z) {
        if (asDouble(z) <= asDouble(b)) {
            return z;
        } else {
            return b;
        }
    }

    /**
     * Maximum function between two objects
     */
    public Object max(Object b, Object z) {
        if (asDouble(z) >= asDouble(b)) {
            return z;
        } else {
            return b;
        }
    }

    /**
     * This function returns an object with a larger value.
     */
    public double Rang(Object b, Object z) {
        if (asDouble(z) >= asDouble(b)) {
            return asDouble(z);
        } else {
            return asDouble(b);
        }
    }
/**
 * Overlap two Interval
 */
    public double similaroverlapfunct(Interval vi, Interval vj) {
        double px = 0.0;
        Interval x = new Interval();
        x.start = max(vi.start, vj.start);
        x.end = min(vi.end, vj.end);
        px = Rang(x.end, x.start);
        return px;
    }

    /**
     * Merge two Interval
     */
    public double similarmergfunct(Interval vi, Interval vj) {
        double px = 0.0;
        Interval x = new Interval();
        x.start = min(vi.start, vj.start);
        x.end = max(vi.end, vj.end);
        px = Rang(x.end, x.start);
        return px;
    }
}
