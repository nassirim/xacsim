/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_pdp;

import java.io.*;

import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author katebi
 */
public class Project_PDP {

    ArrayList< String> listAttrid_Policy = new ArrayList< String>();
    ArrayList< String> listAttrid_request = new ArrayList< String>();
    ArrayList< String> listAttrid_Target = new ArrayList< String>();
    NodeList listpds;

    /**
     * This function collects all the categorical attributes within a rule.
     */
    public void listAttrid_rule(Element eElement) {
        int s1 = eElement.getElementsByTagName("AttributeDesignator").getLength();
        for (int k = 0; k < s1; k++) {
            if (eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string") || eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("anyURI")) {
                listAttrid_Policy.add(eElement.getElementsByTagName("AttributeDesignator").item(k).getAttributes().item(0).getTextContent());
            }
        }
        Object[] st1 = listAttrid_Policy.toArray();
        for (Object s : st1) {
            if (listAttrid_Policy.indexOf(s) != listAttrid_Policy.lastIndexOf(s)) {
                listAttrid_Policy.remove(listAttrid_Policy.lastIndexOf(s));
            }
        }
    }

    /**
     * This function collects all the categorical attributes within the target.
     */
    public void listAttrid_Target(Element eElement) {
        int s1 = eElement.getElementsByTagName("AttributeDesignator").getLength();
        for (int k = 0; k < s1; k++) {
            if (eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string") || eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("anyURI")) {
                listAttrid_Target.add(eElement.getElementsByTagName("AttributeDesignator").item(k).getAttributes().item(0).getTextContent());
            }
        }
        Object[] st1 = listAttrid_Target.toArray();
        for (Object s : st1) {
            if (listAttrid_Target.indexOf(s) != listAttrid_Target.lastIndexOf(s)) {
                listAttrid_Target.remove(listAttrid_Target.lastIndexOf(s));
            }
        }
    }

    /**
     * This function collects all the attributes within the request.
     */
    public void Attrid_req(Element Element__reqset) {
        int s1 = Element__reqset.getElementsByTagName("Attribute").getLength();
        for (int k = 0; k < s1; k++) {
            listAttrid_request.add(Element__reqset.getElementsByTagName("Attribute").item(k).getAttributes().item(0).getTextContent());
        }
        Object[] st1 = listAttrid_request.toArray();
        for (Object s : st1) {
            if (listAttrid_request.indexOf(s) != listAttrid_request.lastIndexOf(s)) {
                listAttrid_request.remove(listAttrid_request.lastIndexOf(s));
            }
        }
    }

    /**
     * A function to evaluate a request with a rule. This function returns a
     * list. This list includes final decisions. Each final decision is based on
     * the comparison of Attributes in the request with the Attributes available
     * in Rule.
     */
    public ArrayList< String> evaluate_R(Element eElement, Element Element__reqset) throws SIMException {
        attrvalue attrv = new attrvalue();
        ArrayList< String> ListDecision = new ArrayList< String>();
        for (int i = 0; i < listAttrid_request.size(); i++) {
            if (listAttrid_Policy.contains(listAttrid_request.get(i))) {
                ArrayList< String> vals_policy = new ArrayList< String>();
                ArrayList< String> vals_request = new ArrayList< String>();
                vals_request.addAll(attrv.request_values(listAttrid_request.get(i), Element__reqset));
                vals_policy.addAll(attrv.ValueS_RuleElement(listAttrid_request.get(i), eElement)[0]);
                if (vals_request.size() <= vals_policy.size()) {
                    for (int j = 0; j < vals_request.size(); j++) {
                        if (!vals_policy.contains(vals_request.get(j))) {
                            ListDecision.add("Not-Applicable");
                            return ListDecision;
                        }
                    }
                } else {
                    ListDecision.add("Not-Applicable");
                }
            }
        }
        return ListDecision;
    }

    /**
     * A function to evaluate a request with a target. This function returns a
     * list. This list includes final decisions. Each final decision is based on
     * the comparison of Attributes in the request with the Attributes available
     * in Target.
     */
    public ArrayList< String> evaluate_T(Element eElement, Element Element__reqset) throws SIMException {
        attrvalue attrv = new attrvalue();
        ArrayList< String> ListDecision = new ArrayList< String>();
        for (int i = 0; i < listAttrid_request.size(); i++) {
            if (listAttrid_Target.contains(listAttrid_request.get(i))) {
                ArrayList< String> vals_policy = new ArrayList< String>();
                ArrayList< String> vals_request = new ArrayList< String>();
                vals_request.addAll(attrv.request_values(listAttrid_request.get(i), Element__reqset));
                vals_policy.addAll(attrv.ValueS_RuleElement(listAttrid_request.get(i), eElement)[0]);
                if (vals_request.size() <= vals_policy.size()) {
                    for (int j = 0; j < vals_request.size(); j++) {
                        if (!vals_policy.contains(vals_request.get(j))) {
                            ListDecision.add("F");
                            return ListDecision;
                        }
                    }
                } else {
                    ListDecision.add("F");
                }
            }
        }
        return ListDecision;
    }

    /**
     * A function to evaluate a request with rules within a policy. This
     * function creates a final decision. This decision is made by applying a
     * request to a policy.
     */
    public String evaluat_Rules(Element element, Element Element__reqset) throws SIMException {
        String Decision_p = "", Decision_T = "", Decision_final = "";
        ArrayList< String> List_Decision_policy = new ArrayList< String>();
        ArrayList< String> List_Decision_policy_T = new ArrayList< String>();
        org.w3c.dom.Node n_target = element.getElementsByTagName("Target").item(0);
        if (n_target.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
            Element element_target = (Element) n_target;
            listAttrid_Target.clear();
            listAttrid_Target(element_target);
            if (!listAttrid_Target.isEmpty()) {
                List_Decision_policy_T.addAll(evaluate_T(element_target, Element__reqset));
            } else {
                List_Decision_policy_T.add("T");
            }
        }
        NodeList listrule = element.getElementsByTagName("Rule");
        for (int temp = 0; temp < listrule.getLength(); temp++) {
            Node nNode = listrule.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                ArrayList< String> List_Decision = new ArrayList< String>();
                listAttrid_Policy.clear();
                listAttrid_rule(eElement);
                if (listAttrid_Policy.isEmpty()) {
                    List_Decision_policy.add(eElement.getAttribute("Effect"));
                } else {
                    List_Decision.addAll(evaluate_R(eElement, Element__reqset));
                    if (List_Decision.contains("Not-Applicable")) {
                        List_Decision_policy.add("Not-Applicable");
                    } else {
                        List_Decision_policy.add(eElement.getAttribute("Effect"));
                    }
                }

            }
        }

        if (element.getAttribute("RuleCombiningAlgId").contains("Permit-Override")) {
            if (List_Decision_policy.contains("Permit")) {
                Decision_p = "Permit";
            } else if (List_Decision_policy.contains("Deny")) {
                Decision_p = "Deny";
            } else {
                Decision_p = "Not-Applicable";
            }

        } else if (element.getAttribute("RuleCombiningAlgId").contains("Deny-Override")) {
            if (List_Decision_policy.contains("Deny")) {
                Decision_p = "Deny";
            } else if (List_Decision_policy.contains("Permit")) {
                Decision_p = "Permit";
            } else {
                Decision_p = "Not-Applicable";
            }

        } else if (element.getAttribute("RuleCombiningAlgId").contains("first-applicable")) {
            for (int k = 0; k < List_Decision_policy.size(); k++) {
                if (!List_Decision_policy.get(k).equals("NotApplicable")) {
                    Decision_p = List_Decision_policy.get(k);
                    k = List_Decision_policy.size() + 1;
                }

            }

        } else if (element.getAttribute("RuleCombiningAlgId").contains("Only-One-Applicable")) {

        }
        // target
        if (List_Decision_policy_T.contains("F")) {
            Decision_T = "F";

        } else {
            Decision_T = "T";
        }

        ///final
        if (Decision_T.equals("T")) {
            Decision_final = Decision_p;
        } else {
            Decision_final = "Not-Applicable";
        }

        return Decision_final;
    }

    /**
     * A function to evaluate each request with two policies. This function
     * creates a final decision. This decision is due to the apply of all
     * requests to the two parties.
     */
    public void evaluate_policy(NodeList policies, NodeList listrequestset, BufferedWriter bw) throws SIMException {
        for (int i1 = 0; i1 < (policies.getLength()); i1 = i1 + 2) {
            for (int j1 = i1 + 1; j1 < (policies.getLength()); j1++) {
                org.w3c.dom.Node ni = policies.item(i1);
                if (ni.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    org.w3c.dom.Node nj = policies.item(j1);
                    if (nj.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                        Element element_p1 = (Element) ni;
                        Element element_p2 = (Element) nj;
                        ArrayList< String> List_Decision_policy1 = new ArrayList< String>();
                        ArrayList< String> List_Decision_policy2 = new ArrayList< String>();
                        double counter_equal_decision = 0;
                        //request
                        for (int reqset_count = 0; reqset_count < listrequestset.getLength(); reqset_count++) {
                            Node Node_reqset = listrequestset.item(reqset_count);
                            if (Node_reqset.getNodeType() == Node.ELEMENT_NODE) {
                                Element Element__reqset = (Element) Node_reqset;
                                listAttrid_request.clear();
                                Attrid_req(Element__reqset);//Collect Attributes_id in Element__reqset
                                String Decision_final_p1 = "", Decision_final_p2 = "";
                                Decision_final_p1 = evaluat_Rules(element_p1, Element__reqset);
                                Decision_final_p2 = evaluat_Rules(element_p2, Element__reqset);
                                List_Decision_policy1.add(Decision_final_p1);
                                List_Decision_policy2.add(Decision_final_p2);
                            }
                        }//end for request
                        double count_all_decision = 0.0;
                        for (int h = 0; h < listrequestset.getLength(); h++) {
                            if (!List_Decision_policy2.get(h).equals("Not-Applicable") || !List_Decision_policy1.get(h).equals("Not-Applicable")) {
                                count_all_decision = count_all_decision + 1;
                                if (List_Decision_policy1.get(h).equals(List_Decision_policy2.get(h))) {
                                    counter_equal_decision = counter_equal_decision + 1;
                                }
                            }
                        }
                        double final_similarity = 0.0;
                        if (count_all_decision != 0) {
                            final_similarity = (counter_equal_decision / count_all_decision);
                        } else {
                            final_similarity = 0.0;
                        }
                        j1 = policies.getLength();
                        try {
                            bw.write((final_similarity) + "\n");
                            bw.flush();

                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }
        }
    }

    /**
     * constructor function Receive request file and policy file from user and
     * check them. Calling the functions of evaluating all requests with two by
     * two of the policies. Make the final decision for each policy pair.
     */
    public Project_PDP() {
        try {
            Scanner scInput = new Scanner(System.in);
            System.out.println("Please enter the full path to input policyset XACML 3.0 (.xml file):");
            String xacmlFilePath = scInput.next();
            Scanner scOutput = new Scanner(System.in);
            System.out.println("Please enter the output file :");
            String xacmlFileName = scOutput.next();
            Scanner sc_req = new Scanner(System.in);
            System.out.println("Please enter the full path to input RequestFile (.xml file):");
            String xacmlFilePath_req = sc_req.next();
            System.out.println();
            System.out.println("-------------------------------------------");
            System.out.println("Please wait!PDP is evaluating policyset and genarating similarity scores... .");
            BufferedWriter bw = null;
            FileWriter fw = null;
            fw = new FileWriter(xacmlFileName);
            bw = new BufferedWriter(fw);

            //Parse policy xacml file
            File xmlFile = new File((xacmlFilePath));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(xmlFile);
            document.getDocumentElement().normalize();

            //Parse request file
            File xmlFile_req = new File((xacmlFilePath_req));
            DocumentBuilderFactory dbf_req = DocumentBuilderFactory.newInstance();
            DocumentBuilder db_req = dbf_req.newDocumentBuilder();
            Document document_req = db_req.parse(xmlFile_req);
            document_req.getDocumentElement().normalize();

            //get nodelist request
            NodeList listrequestset = document_req.getElementsByTagName("Request");

            if (document.getDocumentElement().getNodeName() == "PolicySet") {
                //get nodelist policyset
                NodeList listpolicyset = document.getElementsByTagName("PolicySet");
                for (int policyset_count = 0; policyset_count < listpolicyset.getLength(); policyset_count++) {
                    listpds = null;
                    Node Node_policyset = listpolicyset.item(policyset_count);
                    if (Node_policyset.getNodeType() == Node.ELEMENT_NODE) {
                        Element Element__policyset = (Element) Node_policyset;
                        //get nodelist policy
                        listpds = Element__policyset.getElementsByTagName("Policy");
                        if (listpds.getLength() >= 1 && listrequestset.getLength() >= 1) {
                            evaluate_policy(listpds, listrequestset, bw);
                            fw.close();
                        } else {
                            System.out.println("-------------------------------------------");
                            System.out.println("Error: policy size or request size is zero");
                            System.out.println("-------------------------------------------");
                        }//end else
                    }
                }
                fw.close();
            }//end-if
            else if (listpds.getLength() >= 1 && listrequestset.getLength() >= 1) {
                evaluate_policy(listpds, listrequestset, bw);
                fw.close();
            } else {
                System.out.println("-------------------------------------------");
                System.out.println("Error: policy size or request size is zero");
                System.out.println("-------------------------------------------");
            }//end else
            System.out.println("-------------------------------------------");
            System.out.println("similarity scores Successfully generated!");
            System.out.println("-------------------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//end costructor(Project_PDP())

    /**
     * main function
     */
    public static void main(String[] args) {
        new Project_PDP();
    } //end main
} //end class
