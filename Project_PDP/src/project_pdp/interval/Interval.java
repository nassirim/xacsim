package project_pdp.interval; 
 
 
 import project_pdp.SIMException;
import java.util.ArrayList; 
import java.util.Arrays; 
import java.util.List; 
 
/**
 * @author katebi 
 * @date: 2019 
 */ 
public class Interval<T extends Comparable<T>> { 
    
   private EndPoint<T> lowerBound; 
 
    private boolean lowerBoundClosed = false; 
 
    private EndPoint<T> upperBound; 
 
    private boolean upperBoundClosed = false; 
 
    /**
     * Interval with a single end point 
     * 
     * @param bound 
     */ 
    public Interval(EndPoint<T> bound) { 
        this.lowerBound = this.upperBound = bound; 
        this.lowerBoundClosed = (this.upperBoundClosed = true); 
    } 
 
    public Interval(EndPoint<T> lowerBound, EndPoint<T> upperBound) throws SIMException { 
        this(lowerBound, upperBound, false, false); 
    } 
 
    @SuppressWarnings("unchecked") 
    public Interval(final EndPoint<T> lowerBound, final EndPoint<T> upperBound, boolean isLowerBoundClosed, boolean isUpperBoundClosed) throws SIMException { 
        this.lowerBound = new EndPoint(lowerBound); 
        this.lowerBoundClosed = isLowerBoundClosed; 
 
        this.upperBound = new EndPoint(upperBound); 
        this.upperBoundClosed = isUpperBoundClosed; 
    } 
 
    /**
     * Copy constructor for the immutable class 
     * 
     * @param interval 
     * @throws SIMException 
     */ 
    public Interval(final Interval<T> interval) throws SIMException { 
        this(interval.lowerBound, interval.upperBound, interval.lowerBoundClosed, interval.upperBoundClosed); 
    } 
 
    public Interval(T bound) throws SIMException { 
        this.lowerBound = this.upperBound = new EndPoint<T>(bound); 
        this.lowerBoundClosed = this.upperBoundClosed = true; 
    } 
 
    /**
     * Create an interval with open end points 
     * @param lowerBound 
     * @param upperBound 
     * @throws SIMException 
     */ 
    public Interval(T lowerBound, T upperBound) throws SIMException { 
        this(lowerBound, upperBound, false, false); 
    } 
 
    public Interval(T lowerBound, T upperBound, boolean isLowerBoundClosed, boolean isUpperBoundClosed) throws SIMException { 
        this.lowerBound = new EndPoint<T>(lowerBound); 
        this.upperBound = new EndPoint<T>(upperBound); 
 
        this.lowerBoundClosed = isLowerBoundClosed; 
        this.upperBoundClosed = isUpperBoundClosed; 
    } 
 
    /**
     * Return the complement section of the interval. 
     * 
     * @param op 
     * @return The complemented interval(s), or <CODE>null</CODE> if the complement is empty. 
     */ 
    public List<Interval<T>> complement(final Interval<T> op) throws SIMException { 
 
        if (this.lowerBound.compareTo(op.upperBound) >= 0 || 
            this.upperBound.compareTo(op.lowerBound) <= 0) { 
            Interval<T> newInterval = new Interval<T>(this.lowerBound, this.upperBound); 
 
            if ((this.lowerBound.compareTo(op.upperBound) == 0)) { 
                newInterval.setLowerBoundClosed(this.lowerBoundClosed && !op.upperBoundClosed); 
            } else { 
                newInterval.setLowerBoundClosed(this.lowerBoundClosed); 
            } 
 
            if (this.upperBound.compareTo(op.lowerBound) == 0) { 
                newInterval.setUpperBoundClosed(this.upperBoundClosed && !op.upperBoundClosed); 
            } else { 
                newInterval.setUpperBoundClosed(this.upperBoundClosed); 
            } 
 
            // return null if result is empty 
            if (!newInterval.validate()) { 
                return null; 
            } 
 
            return Arrays.asList(newInterval); 
        } else {    // (this.lowerBound.compareTo(op.upperBound) < 0 && this.upperBound.compareTo(op.lowerBound) > 0) 
            Interval<T> interval1 = new Interval<T>(this.lowerBound, op.lowerBound); 
            Interval<T> interval2 = new Interval<T>(op.upperBound, this.upperBound); 
 
            interval1.setLowerBoundClosed(this.lowerBoundClosed); 
            interval1.setUpperBoundClosed(!op.lowerBoundClosed); 
 
            interval2.setLowerBoundClosed(!op.upperBoundClosed); 
            interval2.setUpperBoundClosed(this.upperBoundClosed); 
 
            List<Interval<T>> result = new ArrayList<>(); 
            if (interval1.validate()) { 
                result.add(interval1); 
            } 
            if (interval2.validate()) { 
                result.add(interval2); 
            } 
            if (result.size() > 0) { 
                return result; 
            } else { 
                return null; 
            } 
        } 
    } 
 
    /**
     * Return true if interval in  the argument is the subset of the current interval. 
     * 
     * @param i 
     * @return 
     */ 
    public boolean contains(final Interval<T> i) { 
 
        int compareLow, compareUp; 
        compareLow = this.lowerBound.compareTo(i.lowerBound); 
        compareUp = this.upperBound.compareTo(i.upperBound); 
 
        if (compareLow < 0) { 
            // check the upper bound 
            if (compareUp > 0) { 
                return true; 
            } else if ((compareUp == 0) && 
                    (this.upperBoundClosed || !i.upperBoundClosed)) { 
                return true; 
            } 
        } else if (compareLow == 0) { 
            if (this.lowerBoundClosed || !i.lowerBoundClosed) { // lowerbound satisfied 
                { 
                    // check upperbound 
                    if (compareUp > 0) { 
                        return true; 
                    } else if ((compareUp == 0) && 
                            (this.upperBoundClosed || !i.upperBoundClosed)) { 
                        return true; 
                    } 
                } 
            } 
        } 
        return false; 
    } 
 
    @Override 
    public boolean equals(Object obj) { 
        if (this == obj) { 
            return true; 
        } 
        if ((obj == null) || 
            (getClass() != obj.getClass())) { 
            return false; 
        } 
 
        @SuppressWarnings("unchecked") 
        Interval<T> other = (Interval<T>) obj; 
 
        if (lowerBound == null) { 
            if (other.lowerBound != null) { 
                return false; 
            } 
        } else if (!lowerBound.equals(other.lowerBound)) { 
            return false; 
        } 
 
        if (lowerBoundClosed != other.lowerBoundClosed) { 
            return false; 
        } 
 
        if (upperBound == null) { 
            if (other.upperBound != null) { 
                return false; 
            } 
        } else if (!upperBound.equals(other.upperBound)) { 
            return false; 
        } 
 
        if (upperBoundClosed != other.upperBoundClosed) { 
            return false; 
        } 
        return true; 
    } 
 
    /**
     * Return the immutable endpoint of the interval 
     * 
     * @return 
     */ 
    public EndPoint<T> getLowerBound() throws SIMException { 
        return new EndPoint<>(this.lowerBound); 
    } 
 
    /**
     * Return the immutable endpoint of the interval. 
     * 
     * @return 
     */ 
    public EndPoint<T> getUpperBound() throws SIMException { 
        return new EndPoint<>(this.upperBound); 
    } 
 
    @Override 
    public int hashCode() { 
        final int prime = 31; 
        int result = 1; 
        result = prime * result 
                + ((lowerBound == null) ? 0 : lowerBound.hashCode()); 
        result = prime * result + (lowerBoundClosed ? 1231 : 1237); 
        result = prime * result 
                + ((upperBound == null) ? 0 : upperBound.hashCode()); 
        result = prime * result + (upperBoundClosed ? 1231 : 1237); 
        return result; 
    } 
 
    /**
     * Check if the value is presenting in the interval. 
     * 
     * @param value 
     * @return 
     */ 
    public boolean hasValue(final T value) throws SIMException { 
 
        //special processing when missing attribute 
        if (value == null) { 
            return this.isLowerInfinite() || this.isUpperInfinite(); 
        } 
 
 
        EndPoint<T> epValue = new EndPoint<>(value); 
 
        int compareLow = this.lowerBound.compareTo(epValue); 
        int compareUp = this.upperBound.compareTo(epValue); 
 
        if ((compareLow < 0 || (compareLow == 0 && this.lowerBoundClosed)) && 
                (compareUp > 0 || (compareUp == 0 && this.upperBoundClosed))) { 
            return true; 
        } 
 
        return false; 
    } 
 
    /**
     * Combine two interval, check if the bounds should be included Bug: not count use-cases when bounds are 
     * infinities. 
     * 
     * @param <T> 
     * @param target 
     * @return 
     */ 
    public Interval<T> includeBound(final Interval<T> target) { 
 
        if (target.lowerBound.equals(target.upperBound)) { // target is a single-value interval 
            if (!this.lowerBound.equals(this.upperBound)) { 
                if (this.upperBound.equals(target.upperBound)) { 
                    this.upperBoundClosed = true; 
                    return this; 
                } else if (this.lowerBound.equals(target.upperBound)) { 
                    this.lowerBoundClosed = true; 
                    return this; 
                } else { 
 
                    throw new RuntimeException("Error! Cannot combine two separated interval"); 
             } 
            } else { 
                throw new RuntimeException("Error! Only support combine single value interval"); 
            } 
        } else if (this.lowerBound.equals(this.upperBound)) { // (*this) is the single-value interval 
            if (target.lowerBound.equals(target.upperBound)) { 
                throw new RuntimeException("Error! Only support combine single value interval"); 
            } 
 
            if (target.lowerBound.equals(this.lowerBound)) { 
                this.lowerBound = target.lowerBound; 
                this.upperBound = target.upperBound; 
                this.lowerBoundClosed = true; 
                this.upperBoundClosed = target.upperBoundClosed; 
 
                return this; 
            } else if (target.upperBound.equals(this.lowerBound)) { 
                this.lowerBound = target.lowerBound; 
                this.upperBound = target.upperBound; 
                this.lowerBoundClosed = target.lowerBoundClosed; 
                this.upperBoundClosed = true; 
 
                return this; 
            } else { 
                throw new RuntimeException("Error! Cannot combine two separated interval"); 
            } 
        } else if (target.lowerBound.equals(this.upperBound)) { 
                this.lowerBound = this.lowerBound; 
                this.upperBound = target.upperBound; 
                this.lowerBoundClosed = this.lowerBoundClosed; 
                this.upperBoundClosed = target.upperBoundClosed; 
 
                return this; 
            }else { 
            throw new RuntimeException("Error! Only support combine single value interval");
           } 
    } 
 
    public boolean isIntersec(final Interval<T> interval) { 
        int c = this.upperBound.compareTo(interval.lowerBound); 
        if (c < 0) { 
            return false; 
        } else if (c == 0) { 
            return this.upperBoundClosed && interval.lowerBoundClosed; 
        } 
        c = this.lowerBound.compareTo(interval.upperBound); 
        if (c > 0) { 
            return false; 
        } else if (c == 0) { 
            return this.lowerBoundClosed && interval.upperBoundClosed; 
        } 
        return true; 
    } 
 
    public boolean isLowerBoundClosed() { 
        return this.lowerBoundClosed; 
    } 
 
    public boolean isLowerInfinite() { 
        return lowerBound.getNegativeInfinity(); 
    } 
 
    public boolean isUpperBoundClosed() { 
        return this.upperBoundClosed; 
    } 
 
    public boolean isUpperInfinite() { 
        return upperBound.getPositiveInfinity(); 
    } 
 
    public void setLowerBound(final EndPoint<T> lowerBound) throws SIMException { 
        this.lowerBound = new EndPoint<>(lowerBound)  ; 
    } 
 
// /** 
//  * Compare two interval if they are the same. 
//  *  
//  * @param interval 
//  * @return 
//  */ 
// public boolean equals(Interval<T> interval) { 
//   
//  boolean lowBoundEqual = false, upBoundEqual = false; 
//   
//  lowBoundEqual = this.lowerBound.equals(interval.lowerBound) && this.lowerBoundClosed == interval.lowerBoundClosed;   
//  upBoundEqual = this.upperBound.equals(interval.upperBound) && this.upperBoundClosed == interval.upperBoundClosed; 
//     
//  return lowBoundEqual && upBoundEqual; 
// } 
 
    public void setLowerBound(final T value) throws SIMException { 
        this.lowerBound = new EndPoint<>(value); 
    } 
 
    public void setLowerBoundClosed(boolean b) { 
        this.lowerBoundClosed = b; 
    } 
 
// public Class<?> getType() { 
//  throw new UnsupportedOperationException("Not support for generic interval type"); 
// } 
 
    public void setLowerInfinite(boolean b) { 
        this.lowerBound.setNegativeInfinity(b); 
        this.lowerBoundClosed = false; 
 
    } 
 
    public void setUpperBound(final EndPoint<T> upperBound) throws SIMException { 
        this.upperBound = new EndPoint<>(upperBound); 
    } 
 
    public void setUpperBound(final T upperBound) throws SIMException { 
        this.upperBound = new EndPoint<>(upperBound); 
    } 
 
    public void setUpperBoundClosed(boolean b) { 
        this.upperBoundClosed = b; 
    } 
 
    public void setUpperInfinite(boolean b) { 
        this.upperBound.setPositiveInfinity(b); 
        this.upperBoundClosed = false; 
    } 
 
    @Override 
    public String toString() { 
        StringBuilder builder = new StringBuilder(); 
 
        if (lowerBoundClosed && upperBoundClosed && lowerBound.equals(this.upperBound)) { 
            builder.append("[" + lowerBound.toString() + "]"); 
        } else { 
            if (this.lowerBound.getNegativeInfinity()) { 
                builder.append("(-inf"); 
            } else { 
                builder.append(this.lowerBoundClosed ? "[" : "("); 
                builder.append(this.lowerBound.toString()); 
            } 
            builder.append(","); 
 
            if (this.upperBound.getPositiveInfinity()) { 
                builder.append("inf)"); 
            } else { 
                builder.append(this.upperBound + (this.upperBoundClosed ? "]" : ")")); 
            } 
        } 
 
        return builder.toString(); 
    } 
 
    /**
     * Check if the interval is valid or an empty interval. 
     * 
     * @return 
     */ 
    public boolean validate() { 
        int compareBound = this.lowerBound.compareTo(this.upperBound); 
 
        return (compareBound < 0) || 
                (compareBound == 0 && this.lowerBoundClosed && this.upperBoundClosed); 
//  if (compareBound < 0) 
//   return true; 
//  else if (compareBound == 0 && this.lowerBoundClosed && this.upperBoundClosed) 
//   return true; 
//  return false; 
    } 
 
    /**
     * The interval contains only 1 value: upper==lower 
     * 
     * @param value 
     */ 
    public void setSingleValue(final EndPoint<T> value) throws SIMException { 
        this.upperBound = this.lowerBound = new EndPoint<>(value); 
        this.lowerBoundClosed = this.upperBoundClosed = true; 
 
    } 
 
    public Class<T> getType() throws SIMException { 
 
        if (this.lowerBound.getType() != null) { 
            return this.lowerBound.getType(); 
        } 
 
        if (this.upperBound.getType() != null) { 
            return this.upperBound.getType(); 
        } 
 
        throw new SIMException("Unsupported (-inf, +inf) interval"); 
    } 
}