package project_pdp;

/**
 * @author katebi
 * Class: define nominal Interval
 */
public class strobj {

    String start; // LowerBound Interval
    String end;  // UpperBound Interval

    /**
     * Constructor function
     */
    public strobj() {

        this.start = "";
        this.end = "";
    }
    /**
     * Constructor function
     * argument:String S, String E
     */
    public strobj(String S, String E) {

        this.start = S;
        this.end = E;
    }

    /**
     * set function
     */
    public void setstrobj(String start, String end) {

        this.start = start;
        this.end = end;

    }
    /**
     * get function
     */
    public strobj getstrobj() {
        strobj h = new strobj();
        h.start = this.start;
        h.end = this.end;
        return h;
    }

    @Override
    public int hashCode() {
        return this.start.length() + this.end.length();
    }

    @Override
    public String toString() {
        String t;
        t = this.start;
        return t;
    }

}
