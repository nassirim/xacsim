package project_pdp; 
 
public class SIMException extends Exception { 
 
    private static final long serialVersionUID = -3180555551853079935L; 
 
    public SIMException(final String string) { 
        super(string); 
    } 
 
    public SIMException(final String s, final Throwable e) { 
        super(s, e); 
    } 
}