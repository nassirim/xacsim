
package xacmlsimtkd;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import xacmlsimtkd.mtree.Nod;

/**
 *
 * @author katebi
 */
public class Treex {

    /**
     * Finding a node in a tree(Node N is root of tree) whose value is equal to string s
     * Returns a Node from the tree
     */
    public Nod findNode(Nod n, String s) {
        if (n.key == s) {
            return n;
        } else {
            for (Nod child : n.children) {
                Nod result = findNode(child, s);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    /**
     * this function calculate distance between two string(s1,s2) in tree
     */
    public double find(Nod root, String s1, String s2) {
        @SuppressWarnings("unchecked")
        Nod node_level_max, node_level_min;
        Nod Node_s1 = findNode(root, s1);
        Nod Node_s2 = findNode(root, s2);
        if (Node_s1 != (null) && Node_s2 != (null)) {
            if (Node_s1.level >= Node_s2.level) {
                node_level_max = Node_s1;
                node_level_min = Node_s2;
            } else {
                node_level_max = Node_s2;
                node_level_min = Node_s1;
            }
            for (int j = 0; j < node_level_max.level; j++) {
                Nod node_j_max = node_level_max.parent;
                if (node_j_max.key.matches(node_level_min.key)) {
                    return Math.abs(node_j_max.level - node_level_min.level);
                } else {
                    Nod node_j_min = node_level_min.parent;
                    while (node_j_min.key != null) {
                       if (node_j_min.key.matches(node_j_max.key)) {
                            return (Math.abs(Math.abs(node_j_min.level - node_level_min.level) + Math.abs(node_j_max.level - node_level_max.level)));
                        }
                        node_j_min = node_j_min.parent;
                    }
                }
            }
        }

        return 0;
    }

}
