
package xacmlsimtkd;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.*;

/**
 *
 * @author katebi
 */

public class mtree {
    
    /**
     * class Node 
     */
 class Nod {
    
        ArrayList<Nod> children = new ArrayList<Nod>(); //Nod children; 
        String key;
        int level;
        Nod parent;

     /**
     * Constructor for class Node 
     */
        Nod(Nod parent, String key, int h) { 
            this.key = key;
            this.level = h;
            this.parent = parent;

        }
    }

     /**
     * Create tree for subject element 
     * Returns the root function of the tree
     */
    public Nod tree_role() {

        Nod root = new Nod(null, "role", 0);
        Nod child1 = new Nod(root, "subviewer", 1);
        Nod child2 = new Nod(root, "pc-member", 1);
        Nod child_child1 = new Nod(child2, "pc-chair", 2);
        Nod child_child2 = new Nod(child2, "admin", 2);
        root.children.add(child1);
        root.children.add(child2);
        child2.children.add(child_child1);
        child2.children.add(child_child2);
        return root;
    }

    /**
     * Create tree for action element 
     * Returns the root function of the tree
     */
    public Nod tree_action() {
        Nod root = new Nod(null, "action", 0);
        Nod child1 = new Nod(root, "read", 1);
        Nod child2 = new Nod(root, "write", 1);
        Nod child3 = new Nod(root, "create", 1);
        Nod child4 = new Nod(root, "delete", 1);
        root.children.add(child1);
        root.children.add(child2);
        root.children.add(child3);
        root.children.add(child4);

        return root;
    }

    /**
     * Create tree for resource element 
     * Returns the root function of the tree
     */
    public Nod tree_resource() {
        Nod root = new Nod(null, "resource", 0);
        Nod child1 = new Nod(root, "conference_rc", 1);
        Nod child2 = new Nod(root, "pcMember_rc", 1);
        Nod child3 = new Nod(root, "paper_rc", 1);
        Nod child4 = new Nod(root, "isMeetingFlag", 1);
        root.children.add(child1);
        root.children.add(child2);
        root.children.add(child3);
        root.children.add(child4);

        Nod child1_child1 = new Nod(child1, "conferenceInfo_rc", 2);
        child1.children.add(child1_child1);
        Nod child2_child1 = new Nod(child2, "pcMember-assignments_rc", 2);
        Nod child2_child2 = new Nod(child2, "pcMember-conflicts_rc", 2);
        Nod child2_child3 = new Nod(child2, "pcMember-info_rc", 2);
        child2.children.add(child2_child1);
        child2.children.add(child2_child2);
        child2.children.add(child2_child3);
        
        Nod child2_child1_child1 = new Nod(child2_child1, "pcMember-assignmentCount_rc", 3);
        Nod child2_child3_child1 = new Nod(child2_child3, "pcMember-info-password_rc", 3);
        Nod child2_child3_child2 = new Nod(child2_child3, "pcMember-info-isChairFlag_rc", 3);
        child2_child1.children.add(child2_child1_child1);
        child2_child3.children.add(child2_child3_child1);
        child2_child3.children.add(child2_child3_child2);
        
        Nod child3_child1 = new Nod(child3, "paper-submission_rc", 2);
        Nod child3_child2 = new Nod(child3, "paper-decision_rc", 2);
        Nod child3_child3 = new Nod(child3, "paper-conflicts_rc", 2);
        Nod child3_child4 = new Nod(child3, "paper-assignments_rc", 2);
        Nod child3_child5 = new Nod(child3, "paper-review_rc", 2);
        child3.children.add(child3_child1);
        child3.children.add(child3_child2);
        child3.children.add(child3_child3);
        child3.children.add(child3_child4);
        child3.children.add(child3_child5);
        
        Nod child3_child1_child1 = new Nod(child3_child1, "paper-submission-info_rc", 3);
        Nod child3_child1_child2 = new Nod(child3_child1, "paper-submission-file_rc", 3);
        child3_child1.children.add(child3_child1_child1);
        child3_child1.children.add(child3_child1_child2);
        Nod child3_child5_child1 = new Nod(child3_child5, "paper-review-info_rc", 3);
        Nod child3_child5_child2 = new Nod(child3_child5, "paper-review-content_rc", 3);
        child3_child5.children.add(child3_child5_child1);
        child3_child5.children.add(child3_child5_child2);
        
        Nod child3_child5_child1_child1 = new Nod(child3_child5_child1, "paper-review-info-submissionStatus_rc", 4);
        Nod child3_child5_child1_child2 = new Nod(child3_child5_child1, "paper-review-info-reviewer_rc", 4);
        child3_child5_child1.children.add(child3_child5_child1_child1);
        child3_child5_child1.children.add(child3_child5_child1_child2);
        Nod child3_child5_child2_child1 = new Nod(child3_child5_child2, "paper-review-content-rating_rc", 4);
        Nod child3_child5_child2_child2 = new Nod(child3_child5_child2, "paper-review-content-commentsAll_rc", 4);
        Nod child3_child5_child2_child3 = new Nod(child3_child5_child2, "paper-review-content-commentsPc_rc", 4);
        child3_child5_child2.children.add(child3_child5_child2_child1);
        child3_child5_child2.children.add(child3_child5_child2_child2);
        child3_child5_child2.children.add(child3_child5_child2_child3);

        return root;
    }

}//end class mtree
