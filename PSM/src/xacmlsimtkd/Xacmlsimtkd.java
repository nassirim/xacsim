
package xacmlsimtkd;

import java.io.*;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.io.*;
import java.util.Scanner;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import xacmlsimtkd.mtree.Nod;

/**
 *
 * @author katebi
 */
public class Xacmlsimtkd {

    NodeList listpds;
    ArrayList< Element> PR1 = new ArrayList< Element>(); //arrayList for  permit rules in policy1
    ArrayList< Element> PR2 = new ArrayList< Element>();//arrayList for  permit rules in policy2
    ArrayList< Element> DR1 = new ArrayList< Element>();//arrayList for  deny rules in policy1
    ArrayList< Element> DR2 = new ArrayList< Element>();//arrayList for  deny rules in policy2
    double epsilon = 0.75;
    ArrayList< Element> phiPR1 = new ArrayList< Element>(); //arrayList for  phi mapping  permit rules in policy1
    ArrayList< Element> phiPR2 = new ArrayList< Element>();//arrayList for  phi mapping permit rules in policy2
    ArrayList< Element> phiDR1 = new ArrayList< Element>();//arrayList for  phi mapping deny rules in policy1
    ArrayList< Element> phiDR2 = new ArrayList< Element>();//arrayList for  phi mapping deny rules in policy2
    Nod roottred1; //tree root for sunject tree
    Nod roottred2;//tree root for resource tree
    Nod roottred3;//tree root for action tree

    int counterattridp1p2;

    
    /**
     * Maximum Function
     */
    public int max(int a, int b) {
        if (a >= b) {
            return a;
        } else {
            return b;
        }
      }

    /**
     * calculate similarity between two target policies (pi,pj)
     */
    public double S_target(Element pi, Element pj) {
        double a = 0;
        Node nListpi = pi.getElementsByTagName("Target").item(0);
        Node nListpj = pj.getElementsByTagName("Target").item(0);
        if (nListpi.getNodeType() == Node.ELEMENT_NODE) {
            if (nListpj.getNodeType() == Node.ELEMENT_NODE) {
                Element eElementpi = (Element) nListpi;
                Element eElementpj = (Element) nListpj;
                a = S(eElementpi, eElementpj);
            }
        }
        return a;
    }

    /**
     * calculate similarity between two nominal values
     */
    public double S_ATTRstring(Element ri, Element rj, String sattr, String Eattr) {
        double attrids = 0;
        Valuesattrid valuesstring = new Valuesattrid();
        attrids = 0.5 * (1 + valuesstring.Extractvalues(ri, rj, sattr, Eattr, roottred1, roottred2, roottred3));
        return attrids;
    }

    /**
     * calculate similarity between two numerical values
     */
    public double S_ATTRnum(Element ri, Element rj, String nattr, String Eattr) {
        double attridn = 0;
        Valuesattrid valuesnum = new Valuesattrid();
        attridn = 0.5 * (1 + valuesnum.Extractvalues(ri, rj, nattr, Eattr, roottred1, roottred2, roottred3));
        return attridn;
    }

    /**
     * this function calculate similarity for elements (subject, resource, action)
     * function return a double value The result of the calculation of similarity similarity a element
     */
    public double Ssrae(Element ri, Element rj, String qt) {//Element_similarity
        double se = 0;
        ArrayList< String> listAttridstringri = new ArrayList< String>(); //arrayList for  deny rules attribute_ids of policy1
        ArrayList< String> listAttridnumri = new ArrayList< String>();
        int s1 = ri.getElementsByTagName(qt + "Match").getLength();
        for (int k = 0; k < s1; k++) {
            if (ri.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string")) {
                listAttridstringri.add(ri.getElementsByTagName(qt + "AttributeDesignator").item(k).getAttributes().item(0).getTextContent());
            }
            if (ri.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("double") || ri.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("int")) {

                listAttridnumri.add(ri.getElementsByTagName(qt + "AttributeDesignator").item(k).getAttributes().item(0).getTextContent());
            }
        }
        ArrayList< String> listAttridstringrj = new ArrayList< String>(); //arrayList for  deny rules attribute_ids of policy1
        ArrayList< String> listAttridnumrj = new ArrayList< String>();
        int s2 = rj.getElementsByTagName(qt + "Match").getLength();

        for (int kg = 0; kg < s2; kg++) {
            if (rj.getElementsByTagName("AttributeValue").item(kg).getAttributes().item(0).getTextContent().contains("string")) {
                listAttridstringrj.add(rj.getElementsByTagName(qt + "AttributeDesignator").item(kg).getAttributes().item(0).getTextContent());

            }
            if (rj.getElementsByTagName("AttributeValue").item(kg).getAttributes().item(0).getTextContent().contains("double") || rj.getElementsByTagName("AttributeValue").item(kg).getAttributes().item(0).getTextContent().contains("int")) {

                listAttridnumrj.add(rj.getElementsByTagName(qt + "AttributeDesignator").item(kg).getAttributes().item(0).getTextContent());

            }
        }
        double Sattributestring = 0, Sattributenum = 0;
        if (listAttridstringri.size() > 0 && listAttridstringrj.size() > 0) {
            for (int h1 = 0; h1 < listAttridstringri.size(); h1++) {
                if (listAttridstringrj.contains(listAttridstringri.get(h1))) {
                    Sattributestring += S_ATTRstring(ri, rj, listAttridstringri.get(h1), qt);
                }
            }
        } else {
            se = 1;

        }
        if (listAttridnumri.size() > 0 && listAttridnumrj.size() > 0) {
            for (int t1 = 0; t1 < listAttridnumri.size(); t1++) {
                if (listAttridnumrj.contains(listAttridnumri.get(t1))) {
                    Sattributenum += S_ATTRnum(ri, rj, listAttridnumri.get(t1), qt);
                }
            }
        } else {
            se = 1;
        }
        int ttr = max((listAttridstringri.size() + listAttridnumri.size()), (listAttridstringrj.size() + listAttridnumrj.size()));
        if (ttr != 0) {
            se = (Sattributestring + Sattributenum) / max((listAttridstringri.size() + listAttridnumri.size()), (listAttridstringrj.size() + listAttridnumrj.size()));
        }
        return se;
    }

    /**
     * calculate similarity of the target rule
     * a target contains subject element, resource element, action element
     */
    public double stargetrule(Element ri, Element rj) {//target_rule
        double t = 0;
        t = (0.33 * Ssrae(ri, rj, "Subject")) + (0.33 * Ssrae(ri, rj, "Resource")) + (0.33 * Ssrae(ri, rj, "Action"));
        return t;
    }

    /**
     * calculate similarity for condition  
     */
    public double sconditionrule(Element ri, Element rj) {//conditio
        double c = 0;

        return c;
    }

    /**
     * calculate similarity between two rules 
     */
    public double S(Element ri, Element rj) {
        double z = 0;
        z = stargetrule(ri, rj)/*+sconditionrule(ri,rj)*/;
        return z;
    }

    /**
     * calculate similarity between two ruleset permit 
     * also calculate similarity between two ruleset deny
     */
    public double S_Ruleset(ArrayList< Element> elist1, ArrayList< Element> elist2, ArrayList< Element> elist3, ArrayList< Element> elist4) {
        double x = 0, k = 0, r = 0;
        for (int i = 0; i < elist1.size(); i++) {
            double y = 0;

            for (int j = 0; j < elist2.size(); j++) {

                double w = S(elist1.get(i), elist2.get(j));
                y += w;
            }
            x += (y / elist2.size());
        }

        for (int i2 = 0; i2 < elist3.size(); i2++) {
            double n = 0;
            for (int j2 = 0; j2 < elist4.size(); j2++) {
                double q = S(elist3.get(i2), elist4.get(j2));
                n += q;
            }
            k += (n / elist4.size());
        }
        r = (x + k) / (elist1.size() + elist3.size());
        return r;
    }

    /**
     * this function calculate mapping for permit ruleset and deny ruleset of policy 1;
     * also calculate mapping for permit ruleset and deny ruleset of policy 2
     */
    public void compute_sim_rule() {
        if (!PR1.isEmpty() && !PR2.isEmpty()) {
            for (int i = 0; i < PR1.size(); i++) {
                for (int j = 0; j < PR2.size(); j++) {
                    double sim_rulePR = S(PR1.get(i), PR2.get(j));
                    if (sim_rulePR >= epsilon) {
                        phiPR1.add(PR2.get(j));
                        phiPR2.add(PR1.get(i));
                    }
                }
            }
        }
        if (!DR1.isEmpty() && !DR2.isEmpty()) {
            for (int t = 0; t < DR1.size(); t++) {
                for (int k = 0; k < DR2.size(); k++) {
                    double sim_ruleDR = S(DR1.get(t), DR2.get(k));
                    if (sim_ruleDR >= epsilon) {
                        phiDR1.add(DR2.get(k));
                        phiDR2.add(DR1.get(t));
                    }
                }
            }
        }

    }
    /**
     * this function categorize permit rules and deny rules in a policy
     * create two Lists of permit and deny rules
     */
    public void categorize_funct(Element element1, Element element2) {
        PR1.clear();
        phiPR1.clear();
        DR1.clear();
        phiDR1.clear();
        PR2.clear();
        phiPR2.clear();
        DR2.clear();
        phiDR2.clear();

        NodeList listrule1 = element1.getElementsByTagName("Rule");
        NodeList listrule2 = element2.getElementsByTagName("Rule");
        for (int temp = 0; temp < listrule1.getLength(); temp++) {
            Node nNode = listrule1.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement1 = (Element) nNode;
                if (eElement1.getAttribute("Effect").matches("Permit")) {
                    PR1.add(eElement1);
                }
                if (eElement1.getAttribute("Effect").matches("Deny")) {

                    DR1.add(eElement1);
                }
            }
        }

        for (int temp2 = 0; temp2 < listrule2.getLength(); temp2++) {
            Node nNode2 = listrule2.item(temp2);
            if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement2 = (Element) nNode2;
                if (eElement2.getAttribute("Effect").matches("Permit")) {
                    PR2.add(eElement2);
                }
                if (eElement2.getAttribute("Effect").matches("Deny")) {

                    DR2.add(eElement2);
                }
            }
        }
    }

    /**
     * this function create three tree for subject, resource, action
     * calculate similarity between two policies
     */
    public void mit(NodeList list, BufferedWriter bw) {
        mtree bm = new mtree();
        roottred1 = bm.tree_role();
        roottred2 = bm.tree_resource();
        roottred3 = bm.tree_action();
        for (int i1 = 0; i1 < (list.getLength()); i1 = i1 + 2) {
            for (int j1 = i1 + 1; j1 < (list.getLength()); j1++) {
                org.w3c.dom.Node n1 = list.item(i1);
                if (n1.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    org.w3c.dom.Node n2 = list.item(j1);
                    if (n2.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                        Element element1 = (Element) n1;
                        Element element2 = (Element) n2;
                        counterattridp1p2 = 0;
                        phiPR1.clear();
                        phiPR2.clear();
                        phiDR1.clear();
                        phiDR1.clear();
                        categorize_funct(element1, element2);
                        compute_sim_rule();
                        double ST = 0.0;
                        ST = S_target(element1, element2);
                        double ruleset_permit = 0.0;
                        if (!phiPR1.isEmpty() || !phiPR2.isEmpty()) {
                            ruleset_permit = S_Ruleset(PR1, phiPR1, PR2, phiPR2);
                        }
                        double ruleset_deny = 0.0;
                        if (!phiDR1.isEmpty() || !phiDR2.isEmpty()) {
                            ruleset_deny = S_Ruleset(DR1, phiDR1, DR2, phiDR2);
                        }
                        double sp1p2 = (0.33 * ST) + (0.33 * ruleset_permit) + (0.33 * ruleset_deny);
                        try {
                            bw.write((sp1p2) + "\n");
                            j1 = list.getLength();

                            bw.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * Constructor function
     * The constructor calls the main functions
     */
    public Xacmlsimtkd() {

        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter the full path to input XACML 2.0 policyset  (.xml file):");
            String xacmlFilePath = sc.next();

            Scanner scoutput = new Scanner(System.in);
            System.out.println("Please enter the output filename :");
            String xacmlFileName = scoutput.next();

            System.out.println("-------------------------------------------");
            System.out.println("Please wait!xacml2_tkde is evaluating policyset and genarating similarity scores ... .");
            System.out.println("-------------------------------------------");
            
            BufferedWriter bw = null;
            FileWriter fw = null;
            fw = new FileWriter(xacmlFileName);
            bw = new BufferedWriter(fw);
            //Pars policyset file
            File xmlFile = new File((xacmlFilePath));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(xmlFile);
            document.getDocumentElement().normalize();

            if (document.getDocumentElement().getNodeName() == "PolicySet") {
                NodeList listpolicyset = document.getElementsByTagName("PolicySet");
                for (int policyset_count = 0; policyset_count < listpolicyset.getLength(); policyset_count++) {
                    listpds = null;
                    Node Node_policyset = listpolicyset.item(policyset_count);
                    if (Node_policyset.getNodeType() == Node.ELEMENT_NODE) {
                        Element Element__policyset = (Element) Node_policyset;
                        listpds = Element__policyset.getElementsByTagName("Policy");
                        if (listpds.getLength() > 1) {
                            this.mit(listpds, bw);
                        } else {
                            System.out.println("PolicySet contains a policy  ...");
                        }
                    }
                }
                System.out.println("-------------------------------------------");
                System.out.println("similarity scores  Successfully generated!");
                System.out.println("-------------------------------------------");
                fw.close();
            } //end-if
            else {
                System.out.println("Enter Second policy ...");
            } //end else
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                new Xacmlsimtkd();
            }
        }).start();
    }

}//end class
