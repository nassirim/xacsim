package xacmlsimtkd;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

/**
 *
 * @author katebi
 */
public class Valuesattrid {

    /**
     * Maximum function
     */
    public double max(double a, double b) {
        if (a >= b) {
            return a;
        } else {
            return b;
        }

    }

    /**
     * Minimum function
     */
    public double min(double a, double b) {
        if (a <= b) {
            return a;
        } else {
            return b;
        }

    }

    /**
     * this function calculate similarity between two string values
     */
    public double S_Two_Value_string(String sv1, String sv2, String Eatt, mtree.Nod roottred1, mtree.Nod roottred2, mtree.Nod roottred3) {
        double v1 = 0;
        Treex m = new Treex();
        if (Eatt.equals("Subject")) {
            if (sv1.equals(sv2)) {
                return 1;
            } else if (roottred1 != null) {
                double mv = m.find(roottred1, sv1, sv2);
                if (mv == 0) {
                    return 0;
                } else {
                    v1 = 1 - (mv / (2));
                    return v1;
                }
            }
        } else if (Eatt.equals("Resource")) {
            if (sv1.equals(sv2)) {
                return 1;
            } else if (roottred2 != null) {
                double mv = m.find(roottred2, sv1, sv2);
                if (mv == 0) {
                    return 0;
                } else {
                    v1 = 1 - (mv / (4));
                    return v1;
                }
            }
        } else if (Eatt.equals("Action")) {
            if (sv1.equals(sv2)) {
                return 1;
            } else if (roottred3 != null) {
                double mv = m.find(roottred3, sv1, sv2);
                if (mv == 0) {
                    return 0;
                } else {
                    v1 = 1 - (mv / (1));
                    return v1;
                }
            }
        }
        return v1;
    }

    /**
     * this function calculate similarity between two numerical values
     */
    public double S_Two_Value_num(double nv1, double nv2) {
        double v1 = 0;
        v1 = 1 - ((Math.abs(nv1 - nv2)) / (nv1 + nv2));
        return v1;
    }

    /**
     * this function calculate similarity between Attributes
     */
    public double Extractvalues(Element ri, Element rj, String sattr, String Eattr, mtree.Nod roottred1, mtree.Nod roottred2, mtree.Nod roottred3) {
        ArrayList< String> B = new ArrayList();
        ArrayList< String> Bequa = new ArrayList();
        ArrayList< Double> Bnum = new ArrayList();
        ArrayList<Double> Bequanum = new ArrayList();
        NodeList listElementri = ri.getElementsByTagName(Eattr);
        for (int iallof = 0; iallof < listElementri.getLength(); iallof++) {
            Node nallof = listElementri.item(iallof);
            if (nallof.getNodeType() == Node.ELEMENT_NODE) {
                Element elementri = (Element) nallof;
                int s3 = elementri.getElementsByTagName(Eattr + "AttributeDesignator").getLength();
                for (int j3 = 0; j3 < s3; j3++) {
                    if (elementri.getElementsByTagName(Eattr + "AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(sattr) && (elementri.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("http://www.w3.org/2001/XMLSchema#string") || elementri.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("xpath-node-match") || elementri.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("anyURI"))) {
                        B.add(elementri.getElementsByTagName("AttributeValue").item(j3).getTextContent());
                    } else if (elementri.getElementsByTagName(Eattr + "AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(sattr) && (elementri.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("double"))) {
                        String attrvnum1 = elementri.getElementsByTagName("AttributeValue").item(j3).getTextContent();
                        double f1 = Double.parseDouble(attrvnum1);
                        Bnum.add(f1);
                    }
                }
            }
        }
        NodeList listElementrj = rj.getElementsByTagName(Eattr);
        for (int ia = 0; ia < listElementrj.getLength(); ia++) {
            Node nal = listElementrj.item(ia);
            if (nal.getNodeType() == Node.ELEMENT_NODE) {
                Element elementrj = (Element) nal;
                int s3 = elementrj.getElementsByTagName(Eattr + "AttributeDesignator").getLength();
                for (int j3 = 0; j3 < s3; j3++) {
                    if (elementrj.getElementsByTagName(Eattr + "AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(sattr) && (elementrj.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("string") || elementrj.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("xpath-node-match") || elementrj.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("anyURI"))) {
                        Bequa.add(elementrj.getElementsByTagName("AttributeValue").item(j3).getTextContent());
                    } else if (elementrj.getElementsByTagName(Eattr + "AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(sattr) && (elementrj.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("double"))) {
                        String attrvnum2 = elementrj.getElementsByTagName("AttributeValue").item(j3).getTextContent();
                        double f2 = Double.parseDouble(attrvnum2);
                        Bequanum.add(f2);
                    }
                }
            }
        }
        double zx = 0;
        if (B != null && Bequa != null) {
            ArrayList< String> Bmax, Bmin;
            if (B.size() >= Bequa.size()) {
                Bmax = B;
                Bmin = Bequa;
            } else {
                Bmax = Bequa;
                Bmin = B;
            }
            double[][] matrix1;
            matrix1 = new double[Bmin.size()][Bmax.size()];
            for (int i = 0; i < Bmin.size(); i++) {
                for (int j = 0; j < Bmax.size(); j++) {
                    matrix1[ i][j] = S_Two_Value_string(Bmin.get(i), Bmax.get(j), Eattr, roottred1, roottred2, roottred3);
                }
            }
            for (int ii = 0; ii < Bmax.size(); ii++) {
                boolean b = false, c = false;
                for (int ff = 0; ff < Bmin.size(); ff++) {
                    if (matrix1[ ff][ii] == 1) {
                        c = true;
                        b = false;
                        ff = Bmin.size();
                        ii = ii + 1;
                        zx = zx + 1;
                    }
                }//end ff 
                if (c != true) {
                    if (b != true) {
                        double s_max = 0;
                        for (int hh = 0; hh < Bmin.size(); hh++) {

                            s_max = matrix1[hh][ii];
                            s_max = max(s_max, matrix1[hh][ii]);
                        }
                        zx += s_max;
                        b = true;
                    }//if(b)
                    else {
                        for (int ss = 0; ss < Bmin.size(); ss++) {

                            zx += matrix1[ss][ii];
                        }
                    }//else b
                }//if(c)
            }
        }
        if (Bnum != null && Bequanum != null) {
            ArrayList< Double> Bmax, Bmin;
            if (Bnum.size() >= Bequanum.size()) {
                Bmax = Bnum;
                Bmin = Bequanum;
            } else {
                Bmax = Bequanum;
                Bmin = Bnum;
            }
            double[][] matrix1;
            matrix1 = new double[Bmin.size()][Bmax.size()];
            for (int i = 0; i < Bmin.size(); i++) {
                for (int j = 0; j < Bmax.size(); j++) {
                    matrix1[ i][j] = S_Two_Value_num(Bmin.get(i), Bmax.get(j));
                }
            }
            for (int ii = 0; ii < Bmax.size(); ii++) {
                boolean b = false, c = false;
                for (int ff = 0; ff < Bmin.size(); ff++) {
                    if (matrix1[ ff][ii] == 1) {
                        c = true;
                        b = false;
                        ff = Bmin.size();
                        ii = ii + 1;
                        zx = zx + 1;
                    }
                }//end ff 
                if (c != true) {
                    if (b != true) {
                        double s_max = 0;
                        for (int hh = 0; hh < Bmin.size(); hh++) {

                            s_max = matrix1[hh][ii];
                            s_max = max(s_max, matrix1[hh][ii]);
                        }
                        zx += s_max;
                        b = true;
                    }//if(b)
                    else {
                        for (int ss = 0; ss < Bmin.size(); ss++) {
                            zx += matrix1[ss][ii];
                        }
                    }//else b
                }//if(c)
            }
        }
        return zx;
    }

}
