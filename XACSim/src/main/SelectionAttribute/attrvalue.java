/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.SelectionAttribute;
import main.cbpsm.*;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.lang.*;
import java.util.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.Object;
import main.interval.*;
import main.interval.partition.*;
import main.SelectionAttribute.calculatedistance.*;


/**
 *
 * @author katebi
 * A class to extract the values of each attribute
 *
 */
public class attrvalue {

    /**
     * function for casting object to double
     */
    public Double asDouble(Object o) {
        Double val = null;
        if (o instanceof Number) {
            val = ((Number) o).doubleValue();
        }
        return val;
    }

    /**
     * function for casting object to integer
     */
    public Integer asInteger(Object o) {
        Integer val = null;
        if (o instanceof Number) {
            val = ((Number) o).intValue();
        }
        return val;
    }

    /**
     * A function for measuring the minimum between two objects
     */
    public Object min(Object b, Object z) {
        if (asDouble(z) <= asDouble(b)) {
            return z;
        } else {
            return b;
        }
    }

    /**
     * A function for measuring the maximum between two objects
     */
    public Object max(Object b, Object z) {
        if (asDouble(z) >= asDouble(b)) {
            return z;
        } else {
            return b;
        }
    }

    /**
     * Function for intersection two lists of numerical intervals
     */
    public List<Interval> getOverlap(ArrayList<Interval> intervalList1, ArrayList<Interval> intervalList2) throws SIMException {

        Partition p1 = new Partition(intervalList1);
        Partition p2 = new Partition(intervalList2);

        Partition p = PartitionBuilder.intersect(p1, p2);
        return p.getIntervals();

    }

    /**
     * Function for union two lists of numerical intervals
     */
    public List<Interval> mergecountinuse(ArrayList<Interval> intervalList1, ArrayList<Interval> intervalList2) throws SIMException {//function union

        Partition p1 = new Partition(intervalList1);
        Partition p2 = new Partition(intervalList2);

        Partition p = PartitionBuilder.union(p1, p2);

        return p.getIntervals();
    }

    /**
     *
     * function for creating the interval of numerical values and its associated
     * operator
     */
    public <T> Interval functintervals1(Element el, int n) throws SIMException {
        if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:integer-greater-than-or-equal") || el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:double-greater-than-or-equal")) {
            String attrvstring2 = el.getElementsByTagName("AttributeValue").item(n).getTextContent();

            double f = Double.parseDouble(attrvstring2);
            Interval<Double> it1 = new Interval<Double>(f);
            it1.setUpperBound(f);
            it1.setLowerBoundClosed(true);
            it1.setLowerInfinite(true);
            return it1;

        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:integer-greater-than") || el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:double-greater-than")) {
            String attrvstring2 = el.getElementsByTagName("AttributeValue").item(n).getTextContent();

            double f = Double.parseDouble(attrvstring2);
            Interval<Double> it1 = new Interval<Double>(f);
            it1.setUpperBound(f);

            it1.setLowerInfinite(true);
            return it1;
        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:integer-less-than-or-equal") || el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:double-less-than-or-equal")) {
            String attrvstring2 = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            double f = Double.parseDouble(attrvstring2);
            Interval<Double> it1 = new Interval<Double>(f);
            it1.setLowerBound(f);
            it1.setLowerBoundClosed(true);
            it1.setUpperInfinite(true);
            return it1;

        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:integer-less-than") || el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:double-less-than")) {
            String attrvstring2 = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            double f = Double.parseDouble(attrvstring2);
            Interval<Double> it1 = new Interval<Double>(f);
            it1.setLowerBound(f);

            it1.setUpperInfinite(true);
            return it1;

        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:integer-equal") || el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:double-equal")) {
            String attrvstring2 = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            double f = Double.parseDouble(attrvstring2);

            Interval<Double> it1 = new Interval<Double>(f);

            return it1;
        } else {
            return null;
        }

    }

    /**
     * function for intersection two lists of nominal intervals
     */
    public ArrayList< String> intersectionFunct(ArrayList< String> m, ArrayList< String> ml2) {
        ArrayList< String> ml1 = new ArrayList();

        Object[] st2 = m.toArray();
        for (Object s : st2) {
            if (m.indexOf(s) != m.lastIndexOf(s)) {
                m.remove(m.lastIndexOf(s));
            }

        }

        Object[] sti = ml2.toArray();
        for (Object s : sti) {
            if (ml2.indexOf(s) != ml2.lastIndexOf(s)) {
                ml2.remove(ml2.lastIndexOf(s));
            }

        }
        for (int i = 0; i < m.size(); i++) {

            if (ml2.contains(m.get(i))) {

                ml1.add(m.get(i));
            }

        }
        Object[] stj = ml1.toArray();
        for (Object s : stj) {
            if (ml1.indexOf(s) != ml1.lastIndexOf(s)) {
                ml1.remove(ml1.lastIndexOf(s));
            }

        }
        return ml1;
    }

    /**
     *
     * function for creating the interval of nominal values and its associated
     * operator
     */
    public ArrayList< String> stringfunct(Element el, int n) {
        strobj z = new strobj();

        ArrayList<String> xl = new ArrayList<String>();
        if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().contains("greater-than-or-equal")) {
            z.start = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            z.end = ">=";
            xl.add(z.toString());

        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().contains("greater-than")) {
            z.start = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            z.end = ">";
            xl.add(z.toString());
        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().contains("less-than-or-equal")) {
            z.start = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            z.end = "<=";
            xl.add(z.toString());
        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().contains("less-than")) {
            z.start = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            z.end = "<";
            xl.add(z.toString());

        } else if (el.getElementsByTagName("Match").item(n).getAttributes().item(0).getTextContent().matches("urn:oasis:names:tc:xacml:1.0:function:string-equal")) {
            z.start = el.getElementsByTagName("AttributeValue").item(n).getTextContent();
            z.end = "=";
            xl.add(z.toString());
        } else {
            xl.add(null);
        }

        return xl;
    }

    /**
     * function for extracting the values of a Attribute in a ruleset
     */
    public ArrayList< String> Values_Ruleset(String attrid, ArrayList< Element> Ruleset) {

        ArrayList< String> ml1 = new ArrayList();
        for (int temp = 0; temp < Ruleset.size(); temp++) {
            Element eElement = Ruleset.get(temp);
            ArrayList< String> L1 = new ArrayList();
            ArrayList< String> L2 = new ArrayList();
            ArrayList< String> L3 = new ArrayList();
            ArrayList< String> L4 = new ArrayList();

            int count = 0;

            NodeList listAnyof = eElement.getElementsByTagName("AnyOf");
            for (int ianyof = 0; ianyof < listAnyof.getLength(); ianyof++) {
                Node nanyof = listAnyof.item(ianyof);
                if (nanyof.getNodeType() == Node.ELEMENT_NODE) {
                    Element elementAnyof = (Element) nanyof;
                    ArrayList< String> A = new ArrayList();
                    A.clear();

                    NodeList listAllof = elementAnyof.getElementsByTagName("AllOf");
                    for (int iallof = 0; iallof < listAllof.getLength(); iallof++) {
                        Node nallof = listAllof.item(iallof);
                        if (nallof.getNodeType() == Node.ELEMENT_NODE) {
                            Element elementAllof = (Element) nallof;

                            int s3 = elementAllof.getElementsByTagName("AttributeDesignator").getLength();
                            ArrayList< String> B = new ArrayList();
                            int num = 0;
                            B.clear();
                            for (int j3 = 0; j3 < s3; j3++) {
                                if (elementAllof.getElementsByTagName("AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(attrid) && (elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("string") || elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("xpath-node-match") || elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("anyURI"))) {

                                    num = num + 1;
                                    if (num >= 2) {
                                        L4.clear();
                                        L4.addAll(stringfunct(elementAllof, j3));
                                        L3.addAll(intersectionFunct(L4, B));
                                        B.clear();

                                        B.addAll(L3);
                                        L3.clear();
                                    } else {
                                        B.addAll(stringfunct(elementAllof, j3));

                                    }
                                }
                            }
                            if (!B.isEmpty()) {
                                A.addAll(B);
                            }

                        }
                    }//end-allof 
                    if (!A.isEmpty()) {
                        count = count + 1;
                    }

                    if (count >= 2) {
                        L1.clear();
                        L1.addAll(A);
                        A.clear();
                        L2.clear();
                        L2.addAll(intersectionFunct(L1, ml1));
                        ml1.clear();
                        ml1.addAll(L2);
                    } else {

                        ml1.addAll(A);

                    }
                }
            }  //end-anyof 

            Object[] st2 = ml1.toArray();
            for (Object s : st2) {
                if (ml1.indexOf(s) != ml1.lastIndexOf(s)) {
                    ml1.remove(ml1.lastIndexOf(s));
                }

            }

        }
        return ml1;
    }

    /**
     * function for extracting the values of a Attribute in a rule
     */
    public List[] ValueS_RuleElement(String attrid, Element element) throws SIMException {
        ArrayList< String> ml1 = new ArrayList< String>();
        ArrayList< Interval> ml1else = new ArrayList< Interval>();
        ArrayList<Interval> L2 = new ArrayList<Interval>();
        ArrayList<Interval> L3 = new ArrayList<Interval>();
        ArrayList<String> L5 = new ArrayList<String>();
        ArrayList<String> L6 = new ArrayList<String>();
        ArrayList<String> L7 = new ArrayList<String>();
        ArrayList<String> L8 = new ArrayList<String>();
        int count = 0;
        int countelse = 0;
        NodeList listAnyof = element.getElementsByTagName("AnyOf");
        for (int ianyof = 0; ianyof < listAnyof.getLength(); ianyof++) {
            Node nanyof = listAnyof.item(ianyof);
            if (nanyof.getNodeType() == Node.ELEMENT_NODE) {
                Element elementAnyof = (Element) nanyof;
                ArrayList< String> A = new ArrayList();
                ArrayList<Interval> Aelse = new ArrayList<Interval>();
                ArrayList<Interval> Aelseint = new ArrayList<Interval>();
                A.clear();
                Aelse.clear();
                int shomarande = 0;
                NodeList listAllof = elementAnyof.getElementsByTagName("AllOf");
                for (int iallof = 0; iallof < listAllof.getLength(); iallof++) {
                    Node nallof = listAllof.item(iallof);
                    if (nallof.getNodeType() == Node.ELEMENT_NODE) {
                        Element elementAllof = (Element) nallof;

                        int s3 = elementAllof.getElementsByTagName("AttributeDesignator").getLength();
                        ArrayList< String> B = new ArrayList();
                        ArrayList<Interval> Belse = new ArrayList<Interval>();
                        ArrayList<Interval> Belsech = new ArrayList<Interval>();
                        int num = 0;
                        int numelse = 0;
                        B.clear();
                        Belse.clear();
                        for (int j3 = 0; j3 < s3; j3++) {
                            if (elementAllof.getElementsByTagName("AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(attrid) && (elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("string") || elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("xpath-node-match") || elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("anyURI"))) {

                                num = num + 1;

                                if (num >= 2) {
                                    L6.clear();
                                    L6.addAll(stringfunct(elementAllof, j3));
                                    L7.addAll(intersectionFunct(L6, B));
                                    B.clear();

                                    B.addAll(L7);
                                    L7.clear();
                                } else {
                                    B.addAll(stringfunct(elementAllof, j3));

                                }

                            } else if (elementAllof.getElementsByTagName("AttributeDesignator").item(j3).getAttributes().item(0).getTextContent().matches(attrid) && (elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("double") || elementAllof.getElementsByTagName("AttributeValue").item(j3).getAttributes().item(0).getTextContent().contains("int"))) {
                                String M = elementAllof.getElementsByTagName("AttributeValue").item(j3).getTextContent();
                                numelse = numelse + 1;
                                if (numelse >= 2) {
                                    L3.clear();
                                    L3.add(functintervals1(elementAllof, j3));
                                    Belsech.addAll(Belse);
                                    Belse.clear();
                                    Belse.addAll(getOverlap(L3, Belsech));
                                } else {

                                    Belse.add(functintervals1(elementAllof, j3));
                                }
                            }
                        }//end-match
                        if (!B.isEmpty()) {
                            A.addAll(B);
                        }
                        if (!Belse.isEmpty()) {
                            shomarande = shomarande + 1;
                        }

                        if (shomarande >= 2) {

                            Aelse.addAll(mergecountinuse(Aelse, Belse));

                        } else {

                            Aelse.addAll(Belse);
                        }
                    }
                }//end-allof

                if (!A.isEmpty()) {

                    count = count + 1;//number of anyof
                }
                if (!Aelse.isEmpty()) {

                    countelse = countelse + 1;
                }
                if (count >= 2) {
                    L5.clear();
                    L5.addAll(A);
                    A.clear();
                    L8.clear();
                    L8.addAll(intersectionFunct(L5, ml1));
                    ml1.clear();
                    ml1.addAll(L8);
                } else {

                    ml1.addAll(A);

                }

                L2.clear();
                L2.addAll(Aelse);
                if (countelse >= 2) {
                    ml1else.addAll(getOverlap(L2, ml1else));

                } else {
                    ml1else.addAll(L2);

                }

            }
        }   //end-anyof 

        Object[] st2 = ml1.toArray();
        for (Object s : st2) {
            if (ml1.indexOf(s) != ml1.lastIndexOf(s)) {
                ml1.remove(ml1.lastIndexOf(s));
            }

        }
        Object[] str = ml1else.toArray();
        for (Object s : str) {
            if (ml1else.indexOf(s) != ml1else.lastIndexOf(s)) {
                ml1else.remove(ml1else.lastIndexOf(s));
            }

        }

        return new List[]{ml1, ml1else};
    }
}
