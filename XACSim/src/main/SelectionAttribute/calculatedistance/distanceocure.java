/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.SelectionAttribute.calculatedistance;
import main.cbpsm.*;
import main.SelectionAttribute.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author katebi 
 * Class to calculate the distance
 */
public class distanceocure {

    coocure tempCar1 = null;

    coocure tempoc1 = null;
    
    /**
     * Function to convert object to double
     */
    public Double asDouble(Object o) {
        Double val = null;
        if (o instanceof Number) {
            val = ((Number) o).doubleValue();
        }
        return val;
    }

    /**
     * Function to calculate the distance between two nominal values
     */
    public double distance(String xi, String xj, ArrayList< String> m1, ArrayList< String> m2, HashMap< String, Integer> hmp2, Element element, Element element1) throws SIMException {
        attrvalue attrval = new attrvalue();

        double distancevalues = 0.0;
        for (int k = 0; k < m1.size(); k++) {
            for (int m = 0; m < m2.size(); m++) {

                ArrayList< String> p1val = new ArrayList();
                ArrayList< String> p2val = new ArrayList();

                if (m2.get(m).matches(m1.get(k))) {
                    ArrayList[] pta1 = new ArrayList[2];
                    p1val.addAll(attrval.ValueS_RuleElement(m1.get(k), element)[0]);

                    p2val.addAll(attrval.ValueS_RuleElement(m1.get(k), element1)[0]);
                    m = m2.size();

                    double dist1;
                    double dist2;
                    for (int h3 = 0; h3 < p1val.size(); h3++) {
                        dist1 = 0.0;
                        dist2 = 0.0;

                        String s = xi + "\t" + " " + p1val.get(h3);
                        String sb = p1val.get(h3) + "\t" + " " + xi;
                        double op1 = 0.0, op2 = 0.0;
                        double opx1 = 0.0, opx2 = 0.0;
                        String y = p1val.get(h3);
                        Set set2 = hmp2.entrySet();
                        Iterator iterator2 = set2.iterator();
                        while (iterator2.hasNext()) {
                            Map.Entry mentry2 = (Map.Entry) iterator2.next();

                            String g = (String) mentry2.getKey();

                            if (g.matches(s)) {

                                op1 = asDouble(mentry2.getValue());
                                Iterator iteratorf = set2.iterator();
                                while (iteratorf.hasNext()) {
                                    Map.Entry mentryf = (Map.Entry) iteratorf.next();

                                    String gf = (String) mentryf.getKey();
                                    if (gf.matches(y)) {

                                        op2 = asDouble(mentryf.getValue());

                                    }

                                }
                                if (op2 != 0.0) {
                                    dist1 = op1 / op2;
                                }
                            }
                            if (g.matches(sb)) {

                                opx1 = asDouble(mentry2.getValue());
                                Iterator iteratorxf = set2.iterator();
                                while (iteratorxf.hasNext()) {
                                    Map.Entry mentryxf = (Map.Entry) iteratorxf.next();

                                    String gxf = (String) mentryxf.getKey();
                                    if (gxf.matches(xi)) {

                                        opx2 = asDouble(mentryxf.getValue());

                                    }

                                }
                                if (opx2 != 0.0) {
                                    dist1 = opx1 / opx2;
                                }
                            }

                        }

                        for (int h4 = 0; h4 < p2val.size(); h4++) {
                            dist2 = 0.0;

                            if (p2val.get(h4).matches(p1val.get(h3))) {

                                String s1 = xj + "\t" + " " + p1val.get(h3);
                                String sb1 = p1val.get(h3) + "\t" + " " + xj;
                                double op3 = 0.0, op4 = 0.0, opz3 = 0.0, opz4 = 0.0;
                                String y1 = p1val.get(h3);
                                Set set1 = hmp2.entrySet();
                                Iterator iterator1 = set1.iterator();
                                while (iterator1.hasNext()) {
                                    Map.Entry mentry1 = (Map.Entry) iterator1.next();

                                    String g1 = (String) mentry1.getKey();

                                    if (g1.matches(s1)) {

                                        op3 = asDouble(mentry1.getValue());
                                        Iterator iterators1 = set1.iterator();
                                        while (iterators1.hasNext()) {
                                            Map.Entry mentrys1 = (Map.Entry) iterators1.next();

                                            String gs1 = (String) mentrys1.getKey();
                                            if (gs1.matches(y1)) {

                                                op4 = asDouble(mentrys1.getValue());

                                            }

                                        }

                                        if (op4 != 0.0) {
                                            dist2 = op3 / op4;
                                        }
                                    }

                                    if (g1.matches(sb1)) {

                                        opz3 = asDouble(mentry1.getValue());
                                        Iterator iteratorsz1 = set1.iterator();
                                        while (iteratorsz1.hasNext()) {
                                            Map.Entry mentrysz1 = (Map.Entry) iteratorsz1.next();

                                            String gsz1 = (String) mentrysz1.getKey();
                                            if (gsz1.matches(xj)) {

                                                opz4 = asDouble(mentrysz1.getValue());

                                            }

                                        }

                                        if (opz4 != 0.0) {
                                            dist2 = opz3 / opz4;
                                        }
                                    }
                                }

                                p2val.remove(p1val.get(h3));
                                h4 = p2val.size();
                            }
                        }

                        distancevalues += Math.pow((dist1 - dist2) * 2, 2);
                    }

                }
            }
        }
        return Math.sqrt((distancevalues));
    }

}
