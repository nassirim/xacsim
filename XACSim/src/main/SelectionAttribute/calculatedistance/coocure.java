/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.SelectionAttribute.calculatedistance;
import main.cbpsm.*;

/**
 *
 * @author katebi
 * class for adjunction nominal values 
 *
 */
public class coocure {

    private String Value_str1;
    private String Value_str2;

    public String getValue_str1() {
        return Value_str1;
    }

    public void setValue_str1(String value_string) {
        this.Value_str1 = value_string;
    }

    public String getValue_str2() {
        return Value_str2;
    }

    public void setValue_str2(String operator_str) {
        this.Value_str2 = operator_str;
    }

    @Override
    public boolean equals(Object obj) {

        if (this.Value_str1.equals(((coocure) obj).getValue_str1()) && this.Value_str2.equals(((coocure) obj).getValue_str2())) {

            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {

        return this.Value_str1.length() + this.Value_str2.length();
    }

    /**
     * function for adjunction two nominal values
     */
    @Override
    public String toString() {

        return this.Value_str1 + "\t " + this.Value_str2;
    }
}
