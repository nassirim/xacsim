/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package main.SelectionAttribute;;
import main.cbpsm.*;

/**
 *
 * @author katebi
 */
public interface RevisionHandler {

  /**
   * Returns the revision string.
   * 
   * @return the revision
   */
  public String getRevision();
}