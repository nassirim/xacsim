/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.SelectionAttribute;
import main.cbpsm.*;

/**
 *
 * @author katebi 
 * A class to define nominal intervals
 * minimum of the range is "start" && maximum of the range is "end" 
 */
public class strobj {

    String start;
    String end;

    public strobj() {

        this.start = "";
        this.end = "";
    }

    public strobj(String S, String E) {

        this.start = S;
        this.end = E;
    }

    public void setstrobj(String start, String end) {

        this.start = start;
        this.end = end;

    }

    public strobj getstrobj() {
        strobj h = new strobj();
        h.start = this.start;
        h.end = this.end;
        return h;
    }

    @Override
    public int hashCode() {

        return this.start.length() + this.end.length();
    }

    /**
     * Function to create a nominal Interval 
     * Create a interval by adjunction value and operator
     */
    @Override
    public String toString() {
        String t;
        t = this.start + " , " + this.end;
        return t;
    }

}
