package main.interval; 
 
import main.cbpsm.*; 
import main.SelectionAttribute.*; 
 
/**
 * @author katebi 
 */ 
public class EndPoint<T extends Comparable<T>> implements Comparable<EndPoint<T>> { 
 
    private boolean fNegativeInfinity; 
 
    private boolean fPositiveInfinity; 
 
    private T value; 
 
    public EndPoint(boolean negativeInfinity, boolean positiveInfinity) { 
        if (!(negativeInfinity ^ positiveInfinity)) { 
            throw new IllegalArgumentException("Only -inf or +inf at a time"); 
        } 
 
        this.fPositiveInfinity = positiveInfinity; 
        this.fNegativeInfinity = negativeInfinity; 
        this.value = null; 
    } 
 
    public EndPoint(T value) throws SIMException { 
        this.fPositiveInfinity = false; 
        this.fNegativeInfinity = false; 
        this.value = GenericUtils.newInstance(value); 
    } 
 
    public EndPoint(EndPoint<T> p) throws SIMException { 
        this.fNegativeInfinity = p.fNegativeInfinity; 
        this.fPositiveInfinity = p.fPositiveInfinity; 
 
        // Perform deep copy 
        this.value = GenericUtils.newInstance(p.value); 
    } 
 
    @Override 
    public int compareTo(EndPoint<T> o) { 
        if (this.fPositiveInfinity) { 
            return o.fPositiveInfinity ? 0 : 1; 
        } else if (this.fNegativeInfinity) { 
            return o.fNegativeInfinity ? 0 : -1; 
        } else { 
            if (o.fPositiveInfinity) { 
                return -1; 
            } else if (o.fNegativeInfinity) { 
                return 1; 
            } else { 
                return this.value.compareTo(o.value); 
            } 
        } 
    } 
 
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object) 
     */ 
    @Override 
    public boolean equals(Object obj) { 
        if (this == obj) { 
            return true; 
        } 
 
        if (!(obj instanceof EndPoint)) { 
            return false; 
        } 
 
        EndPoint<T> other = (EndPoint<T>) obj; 
 
        if ((fNegativeInfinity != other.fNegativeInfinity) || (fPositiveInfinity != other.fPositiveInfinity)) { 
            return false; 
        } 
        if (value == other.value) { 
            return true; 
        } 
 
        if (value != null) { 
            return value.equals(other.value); 
        } 
        return false; 
    } 
 
    public boolean getNegativeInfinity() { 
        return this.fNegativeInfinity; 
    } 
 
    public boolean getPositiveInfinity() { 
        return this.fPositiveInfinity; 
    } 
 
    /* (non-Javadoc)
  * @see java.lang.Object#hashCode() 
  */ 
    @Override 
    public int hashCode() { 
        final int prime = 31; 
        int result = 1; 
        result = prime * result + (fNegativeInfinity ? 1231 : 1237); 
        result = prime * result + (fPositiveInfinity ? 1231 : 1237); 
        result = prime * result + ((value == null) ? 0 : value.hashCode()); 
        return result; 
    } 
 
    public void setNegativeInfinity(boolean b) { 
        this.fPositiveInfinity = false; 
        this.fNegativeInfinity = b; 
        this.value = null; 
    } 
 
    public void setPositiveInfinity(boolean b) { 
        this.fPositiveInfinity = b; 
        this.fNegativeInfinity = false; 
        this.value = null; 
    } 
 
    @SuppressWarnings("unchecked") 
    @Override 
    public String toString() { 
        return value.toString(); 
    } 
 
    public Class<T> getType() { 
 
        if (this.value != null) { 
            return (Class<T>) value.getClass(); 
        } else { 
            return null; 
        } 
    } 
}