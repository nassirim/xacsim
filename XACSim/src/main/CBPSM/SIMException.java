/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package main.cbpsm;

/**
 *
 * @author katebi
 * Similarity Exception
 */
public class SIMException extends Exception { 
 
    private static final long serialVersionUID = -3180555551853079935L; 
 
    public SIMException(final String string) { 
        super(string); 
    } 
 
    public SIMException(final String s, final Throwable e) { 
        super(s, e); 
    } 
}