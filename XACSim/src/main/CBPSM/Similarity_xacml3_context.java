/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.cbpsm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import main.SelectionAttribute.*;
import main.interval.*;
import main.interval.partition.*;
import java.util.*;
import main.SelectionAttribute.calculatedistance.*;
/**
 *
 * @author katebi
 */
public class Similarity_xacml3_context {

    NodeList listpds;
    String filename = "Cp3.xml";
    

    HashMap< String, Integer> hmp1 = new HashMap< String, Integer>(); //Hashmap to store the number of times the pair Permit-values are repeated 
    HashMap< String, Integer> hmp2 = new HashMap< String, Integer>(); //Hashmap to store the number of times the pair Deny-values are repeated   

    ArrayList< String> listAttridPR = new ArrayList< String>();
    ArrayList< String> listAttridDR = new ArrayList< String>();

    ArrayList< Element> Permit__Ruleset = new ArrayList<Element>();
    ArrayList< Element> Deny__Ruleset = new ArrayList< Element>();
    ArrayList< Element> Target_set = new ArrayList< Element>();
    int CounterMemUse = 0, CounterTimeUse = 0;
    int CounteContext=0;
    long MemUsed2, TimeUse;
    String OutSimOrDis;
    double TheresholdNominal;
    BufferedWriter BufContextWrite ;
    FileWriter FilContextWrite ;

    /**
     * A function for calculating the maximum between two integer
     */
    public int max(int a, int b) {
        if (a >= b) {
            return a;
        } else {
            return b;
        }

    }

    /**
     * A function for calculating the maximum between two double
     */
    public double maxvalue(double x, double y) {
        if (x >= y) {
            return x;
        } else {
            return y;
        }

    }

    /**
     * A function for calculating the minimum between two double
     */
    public double minvalue(double x, double y) {
        if (x <= y) {
            return x;
        } else {
            return y;
        }

    }

    /**
     * A function for calculating the similarity score between two targets
     */
    public double[] S_target(Element pi, Element pj, HashMap< String, Integer> HM, ArrayList< Element> TargetSet) throws SIMException {
        double[] targetpolicy = new double[2];

        Node nListpi = pi.getElementsByTagName("Target").item(0);
        Node nListpj = pj.getElementsByTagName("Target").item(0);
        if (nListpi.getNodeType() == Node.ELEMENT_NODE) {
            if (nListpj.getNodeType() == Node.ELEMENT_NODE) {
                Element eElementpi = (Element) nListpi;
                Element eElementpj = (Element) nListpj;
                targetpolicy = S(eElementpi, eElementpj, HM, TargetSet);
            }
        }
        return targetpolicy;
    }

    /**
     * function for casting object to double
     */
    public Double asDouble(Object o) {
        Double val = null;
        if (o instanceof Number) {
            val = ((Number) o).doubleValue();
        }
        return val;
    }

    public double Rang(List<Interval<Double>> List) throws SIMException {
        double rangvinterval = 0.0;
        if (!List.isEmpty()) {
            for (int i = 0; i < List.size(); i++) {
                Interval IntervalList = List.get(i);
                if (!IntervalList.isLowerInfinite() && !IntervalList.isUpperInfinite()) {
                    rangvinterval += Double.parseDouble(IntervalList.getUpperBound().toString()) - Double.parseDouble(IntervalList.getLowerBound().toString());

                } else {
                    rangvinterval = Double.POSITIVE_INFINITY;

                }

            }
        } else {
            rangvinterval = 0.0;
        }
        return rangvinterval;
    }

    public double functionw2(double rangeunion, double rangeintersect) throws SIMException {
        double px = 0.0;

        px = rangeintersect / rangeunion;
        return px;
    }

    public double functionw3(double rangeintersect) throws SIMException {
        double px = 0.0;

        px = 1 / rangeintersect;
        return px;
    }

    public double functionw4(double rangeunion, double rangeintersect) throws SIMException {
        double px = 0.0;
        if (rangeunion == Double.POSITIVE_INFINITY) {
            if (rangeintersect != Double.POSITIVE_INFINITY) {
                px = Double.POSITIVE_INFINITY;

            } else {
                px = 0.0;
            }
        } else {
            px = rangeunion / rangeintersect;
        }

        return px;
    }

    public double functionw5(List<Interval<Double>> ListIntersect) throws SIMException {
        double px = 0.0;
        double g2interval = 0.0;
        if (!ListIntersect.isEmpty()) {

            Interval<Double> ginterval = ListIntersect.get(0);

            double gintervalupper = Double.parseDouble(ginterval.getUpperBound().toString());
            double gintervallower = Double.parseDouble(ginterval.getLowerBound().toString());

            if (gintervalupper == Double.POSITIVE_INFINITY || gintervalupper == Double.NEGATIVE_INFINITY || gintervallower == Double.POSITIVE_INFINITY || gintervallower == Double.NEGATIVE_INFINITY) {
                g2interval = Double.POSITIVE_INFINITY;

                px = 1 / g2interval;
            } else {
                g2interval = (gintervalupper - gintervallower) / ((gintervalupper + gintervallower) - (gintervalupper - gintervallower));
                px = 1 / g2interval;

            }
        } else {
            px = 1 / g2interval;
        }
        return px;
    }
    /*
     measurement the similarity and distance at the level of the value-numerical
    
     */

    public double similaroverlapfunct(Interval vi, Interval vj) throws SIMException {
        double px = 0.0;
        Interval x = new Interval(0.0, 0.0, false, false);
        List<Interval<Double>> LVI = new ArrayList();
        List<Interval<Double>> LVJ = new ArrayList();
        List<Interval<Double>> ListIntersect = new ArrayList();
        LVI.add(vi);
        LVJ.add(vj);

        Partition<Double> p1 = new Partition<Double>(LVI);
        Partition<Double> p2 = new Partition<Double>(LVJ);

        Partition<Double> pIntersect = PartitionBuilder.intersect(p1, p2);
        ListIntersect = pIntersect.getIntervals();

        /*way_2
       
         px=functionw2(rangeintersect,rangeunion);
         */
        /*way_3 
        
         px=functionw3(rangeintersect);
         */
        /*way_4   
         px = functionw4(rangeunion, rangeintersect);
         */
        /*way_5  */
        px = functionw5(ListIntersect);
        return px;
    }
    /*
     This function calculates the similarity and distance for nominal features
     And measures the similarity and distance at the level of the Attribute.
     */

    public double[] S_ATTRstring(Element ri, Element rj, ArrayList<String> liststring_ri, ArrayList<String> liststring_rj, HashMap< String, Integer> hashmap, ArrayList< Element> Ruleset) throws SIMException {

        double[] attrids = new double[2];
        double permitrulesDiscat = 0.0;
        attrvalue attrval = new attrvalue();
        attrvalue attrv = new attrvalue();
        distanceocure distanceattr = new distanceocure();
        double similarityrulePR = 0.0;
        int numdist = 0;
        int nmycounter = 0;

        int bmycounter = 0;

        int counterattributes = 0;

        policyPR1 funcPR1 = new policyPR1();

        if (liststring_ri.isEmpty() || liststring_rj.isEmpty()) {
            similarityrulePR = 0.0;

        } //if two or one of the Lists is empty then  similarity score will be 0
        else if (liststring_ri.size() == 1 || liststring_rj.size() == 1) {
            nmycounter = nmycounter + 1;
            double similarityrulePRcat = 0.0;

            ArrayList<String> rp1val = new ArrayList();
            ArrayList<String> rp2val = new ArrayList();
            if (liststring_ri.size() == 1) {//
                int i = 0;
                if (liststring_rj.contains(liststring_ri.get(i))) {

                    rp1val.addAll(attrval.ValueS_RuleElement(liststring_ri.get(i), ri)[0]);
                    rp2val.addAll(attrval.ValueS_RuleElement(liststring_ri.get(i), rj)[0]);

                    for (int h = 0; h < rp1val.size(); h++) {
                        if (rp2val.contains(rp1val.get(h))) {
                            similarityrulePRcat += 1.0;
                            bmycounter = bmycounter + 1;

                        } else {
                            similarityrulePRcat += 0.0;
                        }
                    }//end_for

                    similarityrulePR = (similarityrulePRcat) / max(rp1val.size(), rp2val.size());

                } else {

                    similarityrulePR = 0.0;
                }
            } else {

                int i = 0;
                if (liststring_ri.contains(liststring_rj.get(i))) {

                    rp1val.addAll(attrval.ValueS_RuleElement(liststring_rj.get(i), ri)[0]);
                    rp2val.addAll(attrval.ValueS_RuleElement(liststring_rj.get(i), rj)[0]);

                    for (int h = 0; h < rp1val.size(); h++) {
                        if (rp2val.contains(rp1val.get(h))) {
                            similarityrulePRcat += 1.0;
                            bmycounter = bmycounter + 1;
                        } else {
                            similarityrulePRcat += 0.0;
                        }

                    }//end_for

                    similarityrulePR = (similarityrulePRcat) / max(rp1val.size(), rp2val.size());
                } else {
                    similarityrulePR = 0.0;
                }
            }

        } else {

            ArrayList< String> rpidmax = new ArrayList();
            ArrayList< String> rpidmin = new ArrayList();
            if (liststring_ri.size() <= liststring_rj.size()) {

                rpidmax.addAll(liststring_rj);
                rpidmin.addAll(liststring_ri);
            } else {

                rpidmax.addAll(liststring_ri);
                rpidmin.addAll(liststring_rj);
            }

            for (int j = 0; j < rpidmin.size(); j++) { //start of two policies comparison 
                numdist = 0;

                ArrayList< String> rp1context = new ArrayList(); //context List of permit rule of policy1

                ArrayList< String> rp2context = new ArrayList(); //context List of permit rule of policy2
                ArrayList< String> rp1val = new ArrayList(); //list of values atribute_id of policy p1
                ArrayList< String> rp2val = new ArrayList(); //list of values atribute_id of policy p2
                ArrayList< String> rpvalmax = new ArrayList(); //list of values atribute_id of policy p1
                ArrayList< String> rpvalmin = new ArrayList();
                rp1val.addAll(attrv.ValueS_RuleElement(rpidmin.get(j), ri)[0]);

                rp2val.addAll(attrv.ValueS_RuleElement(rpidmin.get(j), rj)[0]);

                double distanceocuattrPR = 0.0; //distance for attributes
                if (!rp1val.isEmpty() && !rp2val.isEmpty()) {
                    counterattributes = counterattributes + 1;
                    double rulePR1 = 0.0;

                    double simdisrules = 0.0;

                    funcPR1.functcontextPR1(rpidmin.get(j), liststring_ri, rp1context, hashmap, Ruleset, TheresholdNominal);
                    funcPR1.functcontextPR1(rpidmin.get(j), liststring_rj, rp2context, hashmap, Ruleset, TheresholdNominal);
                    
                   /*
                    for(int e=0;e<rp1context.size();e++){
                        if(rp2context.contains(rp1context.get(e))){CounteContext=CounteContext+1;}
                    }
                    try{
                    BufContextWrite.write(CounteContext);
                    BufContextWrite.flush();}catch(Exception e){}
                    */          
                    distanceocuattrPR = 0.0;
                    if (rp1val.size() >= rp2val.size()) {
                        rpvalmax.addAll(rp1val);
                        rpvalmin.addAll(rp2val);
                    } else {

                        rpvalmax.addAll(rp2val);
                        rpvalmin.addAll(rp1val);
                    }
                    for (int i = 0; i < rpvalmax.size(); i++) {

                        if (rpvalmin.contains(rpvalmax.get(i))) {
                            distanceocuattrPR = 0.0;
                            rulePR1 += 1.0;
                            numdist = 0;
                        } else {
                            if (rp1context.isEmpty() && rp2context.isEmpty()) {
                                permitrulesDiscat += 0.0;
                                similarityrulePR += 0.0;
                            } else {
                                for (int o = 0; o < rpvalmin.size(); o++) {
                                    distanceocuattrPR = 0.0;
                                    numdist = numdist + 1;
                                    distanceocuattrPR = distanceattr.distance(rpvalmax.get(i), rpvalmin.get(o), rp1context, rp2context, hashmap, ri, rj); //distance between two attributes
                                    permitrulesDiscat += distanceocuattrPR;
                                    if (distanceocuattrPR != 0) {

                                        simdisrules += (1 / ((distanceocuattrPR) + 1));

                                    }
                                } //end else

                            }

                        }
                    }

                    similarityrulePR += (((rulePR1 + (simdisrules / rpvalmin.size())) / (rpvalmax.size())));
                }

            }//end-j
        }
        attrids[0] = similarityrulePR;
        attrids[1] = permitrulesDiscat;
        return attrids;
    }
    /*
     This function calculates the similarity and distance for numerical features
     */

    public double[] S_ATTRnum(Element ri, Element rj, ArrayList<String> liststring_ri, ArrayList<String> liststring_rj) throws SIMException {

        double[] attridN = new double[2];
        int numbernumattributep = 0;
        double valuenumattributep = 0.0, permitrulesDis = 0.0;
        int numsimtatbighv = 0;

        attrvalue attrv = new attrvalue();
        for (int numeric1 = 0; numeric1 < liststring_ri.size(); numeric1++) {
            if (liststring_rj.contains(liststring_ri.get(numeric1))) {
                numbernumattributep = numbernumattributep + 1;
                ArrayList< Interval> numvalp1 = new ArrayList< Interval>();
                ArrayList< Interval> numvalp2 = new ArrayList< Interval>();

                numvalp1.addAll(attrv.ValueS_RuleElement(liststring_ri.get(numeric1), ri)[1]);
                numvalp2.addAll(attrv.ValueS_RuleElement(liststring_ri.get(numeric1), rj)[1]);
                if (!numvalp1.isEmpty() && !numvalp2.isEmpty()) {

                    numsimtatbighv = numsimtatbighv + 1;
                    Object[] strcatpermit1 = numvalp1.toArray();
                    for (Object s : strcatpermit1) {
                        if (numvalp1.indexOf(s) != numvalp1.lastIndexOf(s)) {
                            numvalp1.remove(numvalp1.lastIndexOf(s));
                        }

                    }

                    Object[] strcatpermit2 = numvalp2.toArray();
                    for (Object s : strcatpermit2) {
                        if (numvalp2.indexOf(s) != numvalp2.lastIndexOf(s)) {
                            numvalp2.remove(numvalp2.lastIndexOf(s));
                        }

                    }
                    double permitrulesDisP1 = 0.0;
                    double permitrulesDisP2 = 0.0;
                    int x = 0;
                    double permitrulesP1 = 0.0;
                    double statbighP1 = 0.0;
                    double permitrulesP2 = 0.0;
                    double statbighP2 = 0.0;
                    for (int c = 0; c < numvalp1.size(); c++) {
                        Interval cx1 = numvalp1.get(c);
                        if (numvalp2.contains(cx1)) {
                            statbighP1 += 1;

                        } else {
                            double maxvalx = 0.0;
                            double minvalxDis = (Double.POSITIVE_INFINITY);
                            for (int cf = 0; cf < numvalp2.size(); cf++) {
                                Interval cx2 = numvalp2.get(cf);
                                double distancenumericvalues = similaroverlapfunct(cx1, cx2);
                                double permitmaxv = Math.exp((-distancenumericvalues));
                                maxvalx = maxvalue(maxvalx, permitmaxv);
                                minvalxDis = minvalue(minvalxDis, distancenumericvalues);
                            }
                            permitrulesP1 += (maxvalx);
                            permitrulesDisP1 += minvalxDis;

                        }
                    }

                    for (int cP2 = 0; cP2 < numvalp2.size(); cP2++) {
                        Interval cx1P2 = numvalp2.get(cP2);
                        if (numvalp1.contains(cx1P2)) {
                            statbighP2 += 1;

                        } else {
                            double maxvalxP2 = 0.0;
                            double minvalxDis = (Double.POSITIVE_INFINITY);
                            for (int cfP2 = 0; cfP2 < numvalp1.size(); cfP2++) {
                                Interval cx2P2 = numvalp1.get(cfP2);
                                double distancenumericvalues = similaroverlapfunct(cx1P2, cx2P2);
                                double permitmaxv = Math.exp((-distancenumericvalues));
                                maxvalxP2 = maxvalue(maxvalxP2, permitmaxv);
                                minvalxDis = minvalue(minvalxDis, distancenumericvalues);
                            }
                            permitrulesP2 += (maxvalxP2);
                            permitrulesDisP2 += minvalxDis;

                        }
                    }

                    permitrulesDis += ((permitrulesDisP2 + permitrulesDisP1) / (numvalp1.size() + numvalp2.size()));

                    valuenumattributep += ((statbighP1 + permitrulesP1 + statbighP2 + permitrulesP2) / (numvalp1.size() + numvalp2.size()));

                }
            }
        }
        attridN[0] = valuenumattributep;
        attridN[1] = permitrulesDis;
        return attridN;
    }

    /**
     * This function separates the type of numerical and nominal attributes in
     * each target, and calculates the similarity and distance between the two
     * targets.
     */
    public double[] stargetrule(Element ri, Element rj, HashMap< String, Integer> hashmap, ArrayList< Element> Ruleset) throws SIMException {
        double se = 0.0, De = 0.0;
        double[] attrids = new double[2];
        double[] attridN = new double[2];
        double[] attridRSN = new double[2];
        ArrayList< String> AttrIdString_ri = new ArrayList< String>(); //arrayList for  String attribute_ids of rule_i
        ArrayList< String> AttrIdString_rj = new ArrayList< String>(); //arrayList for  String attribute_ids of rule_j

        ArrayList< String> AttrIdNumeric_ri = new ArrayList< String>(); //arrayList for  Numeric attribute_ids of rule_i
        ArrayList< String> AttrIdNumeric_rj = new ArrayList< String>(); //arrayList for  Numeric attribute_ids of rule_j

        int s2 = ri.getElementsByTagName("AttributeDesignator").getLength();
        for (int j = 0; j < s2; j++) {
            if (ri.getElementsByTagName("AttributeValue").item(j) != null) {
                if (ri.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("string")) {
                    AttrIdString_ri.add(ri.getElementsByTagName("AttributeDesignator").item(j).getAttributes().item(0).getTextContent());
                }
                if (ri.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("double") || ri.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("int")) {
                    AttrIdNumeric_ri.add(ri.getElementsByTagName("AttributeDesignator").item(j).getAttributes().item(0).getTextContent());
                }
            }
        }

        Object[] st4 = AttrIdString_ri.toArray(); //for remove repeated values
        for (Object s : st4) {
            if (AttrIdString_ri.indexOf(s) != AttrIdString_ri.lastIndexOf(s)) {
                AttrIdString_ri.remove(AttrIdString_ri.lastIndexOf(s));
            }
        }

        Object[] st1 = AttrIdNumeric_ri.toArray(); //for remove repeated values
        for (Object s : st1) {
            if (AttrIdNumeric_ri.indexOf(s) != AttrIdNumeric_ri.lastIndexOf(s)) {
                AttrIdNumeric_ri.remove(AttrIdNumeric_ri.lastIndexOf(s));
            }
        }

        int s3 = rj.getElementsByTagName("AttributeDesignator").getLength();

        for (int j = 0; j < s3; j++) {
            if (rj.getElementsByTagName("AttributeValue").item(j) != null) {
                if (rj.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("string")) {
                    AttrIdString_rj.add(rj.getElementsByTagName("AttributeDesignator").item(j).getAttributes().item(0).getTextContent());
                }
                if (rj.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("double") || rj.getElementsByTagName("AttributeValue").item(j).getAttributes().item(0).getTextContent().contains("int")) {
                    AttrIdNumeric_rj.add(rj.getElementsByTagName("AttributeDesignator").item(j).getAttributes().item(0).getTextContent());
                }
            }
        }

        Object[] st5 = AttrIdString_rj.toArray();
        for (Object s : st5) {
            if (AttrIdString_rj.indexOf(s) != AttrIdString_rj.lastIndexOf(s)) {
                AttrIdString_rj.remove(AttrIdString_rj.lastIndexOf(s));
            }
        }

        Object[] st2 = AttrIdNumeric_rj.toArray();
        for (Object s : st2) {
            if (AttrIdNumeric_rj.indexOf(s) != AttrIdNumeric_rj.lastIndexOf(s)) {
                AttrIdNumeric_rj.remove(AttrIdNumeric_rj.lastIndexOf(s));
            }
        }
        double Sattributestring = 0, Sattributenum = 0, Dattributestring = 0, Dattributenum = 0;
        attrids = S_ATTRstring(ri, rj, AttrIdString_ri, AttrIdString_rj, hashmap, Ruleset);
        Sattributestring = attrids[0];
        Dattributestring = attrids[1];
        attridN = S_ATTRnum(ri, rj, AttrIdNumeric_ri, AttrIdNumeric_rj);
        Sattributenum = attridN[0];
        Dattributenum = attridN[1];

        se = (Sattributestring + Sattributenum) / (max(AttrIdString_ri.size(), AttrIdString_rj.size()) + max(AttrIdNumeric_ri.size(), AttrIdNumeric_rj.size()));
        De = (Dattributestring + Dattributenum) / (max(AttrIdString_ri.size(), AttrIdString_rj.size()) + max(AttrIdNumeric_ri.size(), AttrIdNumeric_rj.size()));
        attridRSN[0] = se;
        attridRSN[1] = De;

        return attridRSN;
    }

    /**
     * This function calculates the similarity and distance between two rules.
     * These two rules have the same effect
     */
    public double[] S(Element ri, Element rj, HashMap< String, Integer> hashmap, ArrayList< Element> Ruleset) throws SIMException {

        double[] attridtarget = new double[2];
        attridtarget = stargetrule(ri, rj, hashmap, Ruleset);

        return attridtarget;
    }

    public void TargetSet_ALL_Policy() {

        for (int tem = 0; tem < listpds.getLength(); tem++) {
            Node node_p = listpds.item(tem);
            if (node_p.getNodeType() == Node.ELEMENT_NODE) {
                Element element1 = (Element) node_p;
                NodeList listrule1 = element1.getElementsByTagName("Target");

                for (int temp = 0; temp < listrule1.getLength(); temp++) {
                    Node nNode = listrule1.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement1 = (Element) nNode;
                        Target_set.add(eElement1);

                    }
                }

            }
        }

    }

    public void categorize_RulSet() {

        for (int tem = 0; tem < listpds.getLength(); tem++) {
            Node node_p = listpds.item(tem);
            if (node_p.getNodeType() == Node.ELEMENT_NODE) {
                Element element1 = (Element) node_p;
                NodeList listrule1 = element1.getElementsByTagName("Rule");

                for (int temp = 0; temp < listrule1.getLength(); temp++) {
                    Node nNode = listrule1.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement1 = (Element) nNode;
                        if (eElement1.getAttribute("Effect").matches("Permit")) {
                            Permit__Ruleset.add(eElement1);

                        }
                        if (eElement1.getAttribute("Effect").matches("Deny")) {

                            Deny__Ruleset.add(eElement1);

                        }

                    }
                }

            }
        }

    }

    /**
     * This function separates the permit and deny rules
     */
    public List[] categorize_funct(Element element1) {

        ArrayList< Element> RP = new ArrayList< Element>(); //arrayList for Storing permit rules in policy
        ArrayList< Element> RD = new ArrayList< Element>(); //arrayList for Storing deny rules in policy

        NodeList listrule1 = element1.getElementsByTagName("Rule");

        for (int temp = 0; temp < listrule1.getLength(); temp++) {
            Node nNode = listrule1.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement1 = (Element) nNode;
                if (eElement1.getAttribute("Effect").matches("Permit")) {
                    RP.add(eElement1);

                }
                if (eElement1.getAttribute("Effect").matches("Deny")) {

                    RD.add(eElement1);

                }

            }
        }

        return new List[]{RP, RD};
    }

    public double[] compute_sim_rule(List< Element> PR1, List< Element> PR2, HashMap< String, Integer> hashmap, ArrayList< Element> Ruleset) throws SIMException {
        /*
         This function calculates the similarity and distance between two sets of rules. 
         Two sets of rules have equal effect.
         Mapping m between two sets of rules is performed in this function.
         */
        double[] sim_dis = new double[2];
        double similaritypermitmyrules = 0.0;
        double Dispermitmyrules = 0.0;
        double rzt1 = 0.0;
        double Disrzt1 = 0.0;
        double[] arr1 = new double[PR2.size()];
        double[] Disarr1 = new double[PR2.size()];
        for (int disarrt = 0; disarrt < PR2.size(); disarrt++) {
            Disarr1[disarrt] = Double.POSITIVE_INFINITY;
        }
        if (!PR1.isEmpty() && !PR2.isEmpty()) {

            for (int i = 0; i < PR1.size(); i++) {
                double rzt2 = 0.0;
                double Disrzt2 = Double.POSITIVE_INFINITY;
                int countermjpermit = 0;

                for (int j = 0; j < PR2.size(); j++) {
                    double[] sim_dispp = new double[2];
                    sim_dispp = S(PR1.get(i), PR2.get(j), hashmap, Ruleset); //Calculates the similarity && Distance between two rules
                    double sim_rulePR = sim_dispp[0];// Similarity between two rules
                    double dis_rulePR = sim_dispp[1];// Distance between two rules
                    double rzt3 = sim_rulePR;
                    double rzr = maxvalue(rzt3, rzt2);
                    rzt2 = rzr;
                    double Disrzt3 = dis_rulePR;
                    double Disrzr = minvalue(Disrzt3, Disrzt2);
                    Disrzt2 = Disrzr;
                    double rzu = maxvalue(rzt3, arr1[countermjpermit]);
                    arr1[countermjpermit] = rzu;
                    double Disrzu = minvalue(Disrzt3, Disarr1[countermjpermit]);
                    Disarr1[countermjpermit] = Disrzu;
                    countermjpermit = countermjpermit + 1;
                }//end for(j)
                rzt1 = rzt1 + rzt2;

                Disrzt1 = Disrzt1 + Disrzt2;

            }//end for(i)

            double xar = 0.0;
            for (int art = 0; art < PR2.size(); art++) {

                xar = xar + arr1[art];
            }

            similaritypermitmyrules = (rzt1 + xar) / (PR1.size() + PR2.size());

            double Disxar = 0.0;
            for (int disart = 0; disart < PR2.size(); disart++) {

                Disxar = Disxar + Disarr1[disart];
            }

            Dispermitmyrules = (Disrzt1 + Disxar) / (PR1.size() + PR2.size());

        }

        sim_dis[0] = similaritypermitmyrules;
        sim_dis[1] = Dispermitmyrules;

        return sim_dis;

    }

    public void AllPolicyAttrid(NodeList list) {
        /*
        
         This function receives the existing policies in the dataset as inputs and places the permit-related Attributes in the listAttridPR list and places the deny-related Attributes in the listAttridDR list.
         */
        for (int i1 = 0; i1 < (list.getLength()); i1++) {

            org.w3c.dom.Node n = list.item(i1);
            if (n.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                Element element = (Element) n;
                NodeList listrule = element.getElementsByTagName("Rule");
                for (int temp = 0; temp < listrule.getLength(); temp++) {
                    Node nNode = listrule.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;

                        int s1 = eElement.getElementsByTagName("AttributeDesignator").getLength();

                        for (int k = 0; k < s1; k++) {
                            if (eElement.getAttribute("Effect").matches("Permit") && eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string")) {
                                listAttridPR.add(eElement.getElementsByTagName("AttributeDesignator").item(k).getAttributes().item(0).getTextContent());

                            }
                            if (eElement.getAttribute("Effect").matches("Deny") && eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string")) {

                                listAttridDR.add(eElement.getElementsByTagName("AttributeDesignator").item(k).getAttributes().item(0).getTextContent());

                            }
                        }
                    }
                }

            }

        }
        Object[] st1 = listAttridPR.toArray();
        for (Object s : st1) {
            if (listAttridPR.indexOf(s) != listAttridPR.lastIndexOf(s)) {
                listAttridPR.remove(listAttridPR.lastIndexOf(s));
            }
        }

        Object[] st2 = listAttridDR.toArray();
        for (Object s : st2) {
            if (listAttridDR.indexOf(s) != listAttridDR.lastIndexOf(s)) {
                listAttridDR.remove(listAttridDR.lastIndexOf(s));
            }
        }

    }

    /**
     * Calculate the number of times the pair of values is repeated in Permit &
     * Deny RuleSet
     */
    public void contextfunct() throws SIMException {

        policyPR1 funcPR1 = new policyPR1(); //instance of policyPR1 class

        funcPR1.funct1(listAttridPR, Permit__Ruleset, hmp1);//Calculate the number of times the pair of values is repeated in Permit RuleSet

        funcPR1.funct1(listAttridDR, Deny__Ruleset, hmp2);//Calculate the number of times the pair of values is repeated in Deny RuleSet

    }

    public void Calculate_sim_policies(NodeList list, BufferedWriter bw) throws SIMException {

        /**
         * This function calculates the similarity and distance between two
         * policies separately with the sum of the components of the "set permit
         * rules" and "deny set rules" and "the two policy target".
         */
        targetpolicy target_p = new targetpolicy();
        HashMap< String, Integer> HMP_T = new HashMap< String, Integer>();
        TargetSet_ALL_Policy();//EXTRACT_ALL_TARGETS
        categorize_RulSet();//CATEGORIZE_RULES(Permit,Deny)
        HMP_T = target_p.HashMap_Target(Target_set);//Calculate_HashMap_TargetPolicy
        AllPolicyAttrid(listpds);//EXTRACT_ALL_Attributes
        contextfunct();

        for (int i1 = 0; i1 < (list.getLength()); i1 = i1 + 2) {

            for (int j1 = i1 + 1; j1 < (list.getLength()); j1++) {
                org.w3c.dom.Node n1 = list.item(i1);
                if (n1.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    org.w3c.dom.Node n2 = list.item(j1);
                    if (n2.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                        long startTime = System.currentTimeMillis();

                        Element element1 = (Element) n1;
                        Element element2 = (Element) n2;
                        List[] categorize_policy_P1 = new List[2];
                        List[] categorize_policy_P2 = new List[2];
                        List< Element> PR1 = new ArrayList< Element>(); //An array for the permit set Attributes in the first policy
                        List< Element> PR2 = new ArrayList< Element>(); //An array for the permit set Attributes in the second policy
                        List< Element> DR1 = new ArrayList< Element>();//An array for the deny set Attributes in the first policy
                        List< Element> DR2 = new ArrayList< Element>();//An array for the deny set Attributes in the second policy
                        categorize_policy_P1 = categorize_funct(element1);
                        categorize_policy_P2 = categorize_funct(element2);
                        PR1 = categorize_policy_P1[0];
                        DR1 = categorize_policy_P1[1];
                        PR2 = categorize_policy_P2[0];
                        DR2 = categorize_policy_P2[1];

                        double[] targetpolicy = new double[2];
                        double[] rulesetpermitpolicy = new double[2];
                        double[] rulesetdenypolicy = new double[2];

                        targetpolicy = S_target(element1, element2, HMP_T, Target_set);//Calculate_Similarity_and_Distance_PolicyTarget

                        double sim_target = targetpolicy[0];
                        double dis_target = targetpolicy[1];
                        long beforeUsedMem = 0;
                        double similarity_permit = 0.0;
                        double Distance_permit = 0.0;
                        if (PR1.isEmpty() && PR2.isEmpty()) {
                            similarity_permit = 1.0;
                            Distance_permit = 0.0;
                        } else {
                            rulesetpermitpolicy = compute_sim_rule(PR1, PR2, hmp1, Permit__Ruleset);//Calculate_Similarity_and_Distance_RuleSet_Permit
                            similarity_permit = rulesetpermitpolicy[0];
                            Distance_permit = rulesetpermitpolicy[1];
                        }

                        double similarity_deny = 0.0;
                        double Distance_deny = 0.0;
                        if (DR1.isEmpty() && DR2.isEmpty()) {
                            similarity_deny = 1.0;
                            Distance_deny = 0.0;
                        } else {
                            rulesetdenypolicy = compute_sim_rule(DR1, DR2, hmp2, Deny__Ruleset);//Calculate_Similarity_and_Distance_RuleSet_Deny
                            similarity_deny = rulesetdenypolicy[0];
                            Distance_deny = rulesetdenypolicy[1];
                        }

                        long afterUsedMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024);
                        double sp1p2_similarity = (0.33 * sim_target) + (0.33 * similarity_permit) + (0.33 * similarity_deny);
                        double sp1p2_Distance = (dis_target) + (Distance_permit) + (Distance_deny);

                        try {
                            if (OutSimOrDis.equals("1")) {
                                bw.write((sp1p2_similarity) + "\n");
                            } else if (OutSimOrDis.equals("2")) {
                                bw.write(sp1p2_Distance + "\n");
                            } else {
                                System.out.println("ERROR:You must enter 1 or 2 to generate similarity or distance. ");
                            }
                            j1 = list.getLength();
                            bw.flush();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                        long endTime = System.currentTimeMillis();

                        long totalTime = endTime - startTime;
                        System.out.println("Time used=" + (totalTime / 1000.0));
                        long actualMemUsed = afterUsedMem - beforeUsedMem;
                        MemUsed2 += actualMemUsed;
                        CounterMemUse = CounterMemUse + 1;
                        if (CounterMemUse == 2) {
                            System.out.println("Memory used=" + (MemUsed2 / 2));
                            CounterMemUse = 0;
                            MemUsed2 = 0;
                        }

                    }

                }

            }
        }
    }

    /**
     * Constructor Class first takes the name of an xml file containing policies
     * and the name of an output file as input from the user, and places the
     * final results in terms of similarity and distance values in the output
     * file. This function checks the entire policy and policy set in the input
     * file
     */
    public Similarity_xacml3_context() {  //Constructor Class

        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter the full path to input policyset (.xml file):");
            String xacmlFilePath = sc.next();
            System.out.println();

            Scanner scoutput = new Scanner(System.in);
            System.out.println("Please enter the output file :");
            String xacmlFileName = scoutput.next();
            System.out.println();

            Scanner SCOutSimOrDis = new Scanner(System.in);
            System.out.println("Please enter 1(for similarity calculation) OR 2(for distance calculation):");
            OutSimOrDis = SCOutSimOrDis.next();
            System.out.println();

            Scanner TH = new Scanner(System.in);
            System.out.println("Please Enter Thereshold:");
            TheresholdNominal = TH.nextDouble();
            System.out.println();
            /*
            Scanner ShowCotext = new Scanner(System.in);
            System.out.println("Please enter the output file name for show number of context:");
            String OutShowContext = ShowCotext.next();
            System.out.println();
            */
            System.out.println("-------------------------------------------");
            System.out.println("*** Input policyset informations ***");
            System.out.println("Input file name: " + xacmlFilePath);
            System.out.println("*** Output  informations ***");
            System.out.println("Output file name: " + xacmlFileName);
            System.out.println("-------------------------------------------");
            System.out.println();
            System.out.println("Please wait!similarity_xacml3_context is evaluating policyset and genarating similarity and distance scores ... .");

            BufferedWriter bw = null;
            FileWriter fw = null;
            fw = new FileWriter(xacmlFileName);
            bw = new BufferedWriter(fw);
            /*
            BufContextWrite = null;
            FilContextWrite = null;
            FilContextWrite = new FileWriter(OutShowContext);
            BufContextWrite = new BufferedWriter(fw);
            */
            File xmlFile = new File(getClass().getResource(xacmlFilePath).getFile());

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(xmlFile);
            document.getDocumentElement().normalize();

            if (document.getDocumentElement().getNodeName().equals("PolicySet")) {
                NodeList listpolicyset = document.getElementsByTagName("PolicySet");

                for (int policyset_count = 0; policyset_count < listpolicyset.getLength(); policyset_count++) {
                    listpds = null;
                    Node Node_policyset = listpolicyset.item(policyset_count);
                    if (Node_policyset.getNodeType() == Node.ELEMENT_NODE) {
                        Element Element__policyset = (Element) Node_policyset;

                        listpds = Element__policyset.getElementsByTagName("Policy");

                        if (listpds.getLength() > 1) {

                            Calculate_sim_policies(listpds, bw);

                        } else {
                            System.out.println("PolicySet contains a policy  ...");
                        }//end_else
                    }//end_if
                }//end_for
                bw.close();
            }//end_if
            else {
                System.out.println("Enter Second policy ...");

            }//end_else
        } catch (Exception e) {
            e.printStackTrace();
        }//end_catch
    }//end_Similarity_xacml3_context()_functin

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         Define the Threads to speed up code execution       
         */
        new Thread(new Runnable() {

            @Override
            public void run() {
                new Similarity_xacml3_context(); //Instance of the class 
            }
        }).start();
    }

}
