/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity_xacml3_context;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.List;
import similarity_xacml3_context.strobj;
import similarity_xacml3_context.interval.*;
import similarity_xacml3_context.partition.*;

/**
 * @author katebi
 * @date: 2019
 * 
 * A class for create HashMap
 * The hashmap keeps the pair jointed string values 
 */
public class targetpolicy {

    coocure tempCar = null;
    coocure tempoc = null;
    ArrayList< String> attr1 = new ArrayList< String>();
    ArrayList< String> attr2 = new ArrayList< String>();
    ArrayList< String> listAttridPR = new ArrayList< String>();
    HashMap< String, Integer> hashmp1 = new HashMap< String, Integer>();
    HashMap< String, Integer> hashmp2 = new HashMap< String, Integer>();
    HashMap< String, Integer> hm = new HashMap< String, Integer>();
    NodeList listpolicy;

    /**
     * using hashmap to keeps the pair jointed string values
     * the pair jointed string values are In two Target Policies.
     */
    public HashMap< String, Integer> HashMap_Target(ArrayList< Element> TargetSet) throws SIMException {
        policyPR1 HM_T = new policyPR1();
        for (int temp = 0; temp < TargetSet.size(); temp++) {
            Element eElement = TargetSet.get(temp);

            int s1 = eElement.getElementsByTagName("AttributeDesignator").getLength();

            for (int k = 0; k < s1; k++) {
                if (eElement.getElementsByTagName("AttributeValue").item(k).getAttributes().item(0).getTextContent().contains("string")) {
                    listAttridPR.add(eElement.getElementsByTagName("AttributeDesignator").item(k).getAttributes().item(0).getTextContent());

                }

            }
        }

        Object[] st1 = listAttridPR.toArray();
        for (Object s : st1) {
            if (listAttridPR.indexOf(s) != listAttridPR.lastIndexOf(s)) {
                listAttridPR.remove(listAttridPR.lastIndexOf(s));
            }
        }
        HM_T.funct1(listAttridPR, TargetSet, hm);

        return hm;
    }

}
