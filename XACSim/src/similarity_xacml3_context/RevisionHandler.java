/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package similarity_xacml3_context;

/**
 *
 * @author katebi
 */
public interface RevisionHandler {

  /**
   * Returns the revision string.
   * 
   * @return the revision
   */
  public String getRevision();
}