package similarity_xacml3_context; 
 
import similarity_xacml3_context.SIMException; 
 
import java.lang.reflect.Constructor; 
import java.lang.reflect.InvocationTargetException; 
 
/**
 * @author katebi 
 * @since 2019
 */ 
public class GenericUtils { 
 
    @SuppressWarnings("unchecked") 
    public static <T> T newInstance(final T value, Class<?> clsType) throws SIMException { 
        if (value == null) { 
            throw new SIMException("Cannot copy null value"); 
        } 
 
        Constructor<?> copyConstructor = null; 
        try { 
            copyConstructor = clsType.getConstructor(clsType); 
            return (T) copyConstructor.newInstance(value); 
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) { 
            throw new SIMException("Failed to construct class " + clsType.toString(), e); 
        } catch (NoSuchMethodException e) { 
            // use shallow copy if no copy constructor is found 
            return value; 
        } 
    } 
 
    /**
     * Create a new object of type <code>T</code>, which is cloned from <code>value</code>. 
     * The equivalent class copy constructor is invoked. 
     * @param value 
     * @param <T> 
     * @return 
     * @throws SIMException 
     */ 
    public static <T> T newInstance(final T value) throws SIMException { 
        if (value == null) { 
//            throw new SIMException("Cannot copy null value"); 
            return null; 
        } else { 
            return newInstance(value, value.getClass()); 
        } 
 
    } 
}