/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package similarity_xacml3_context;

/**
 *
 * @author katebi
 */
public final class SpecialFunctions
  implements RevisionHandler {

  /** Some constants */
  private static double log2 = Math.log(2);

  /**
   * Returns natural logarithm of factorial using gamma function.
   *
   * @param x the value
   * @return natural logarithm of factorial
   */
  public static double lnFactorial(double x){

    return Statistics.lnGamma(x+1);
  }

  /**
   * Returns base 2 logarithm of binomial coefficient using gamma function.
   *
   * @param a upper part of binomial coefficient
   * @param b lower part
   * @return the base 2 logarithm of the binominal coefficient a over b
   */
  public static double log2Binomial(double a, double b) {
    
    if (Utils.gr(b,a)) {
      throw new ArithmeticException("Can't compute binomial coefficient.");
    }
    return (lnFactorial(a)-lnFactorial(b)-lnFactorial(a-b))/log2;
  }

  /**
   * Returns base 2 logarithm of multinomial using gamma function.
   *
   * @param a upper part of multinomial coefficient
   * @param bs lower part
   * @return multinomial coefficient of a over the bs
   */
  public static double log2Multinomial(double a, double[] bs)
       {
    
    double sum = 0;
    int i;
    
    for (i=0;i<bs.length;i++) {
      if (Utils.gr(bs[i],a)) {
	throw 
	  new ArithmeticException("Can't compute multinomial coefficient.");
      } else {
	sum = sum+lnFactorial(bs[i]);
      }
    }
    return (lnFactorial(a)-sum)/log2;
  }
  
  /**
   * Returns the revision string.
   * 
   * @return		the revision
   */
  public String getRevision() {
    return RevisionUtils.extract("$Revision: 5953 $");
  }

}
