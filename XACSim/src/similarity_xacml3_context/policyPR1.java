/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity_xacml3_context;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

/**
 * @author: katebi
 * @Date:2019
 */
public class policyPR1 {

    coocure tempCar = null;
    coocure tempoc = null;

    /**
     * function to casting object to double
     */
    public Double asDouble(Object o) {
        Double val = null;
        if (o instanceof Number) {
            val = ((Number) o).doubleValue();
        }
        return val;
    }

    /**
     * function to Calculating Attribute-Selection
     */
    public void functcontextPR1(String Attridcontext, ArrayList< String> myset1, ArrayList< String> rd1, HashMap< String, Integer> hmap, ArrayList< Element> Ruleset, double TheresholdNominal) {
        ArrayList< String> rp1 = new ArrayList(); //list of values atribute_id of policy p1
        ArrayList< String> rp2 = new ArrayList(); //list of values atribute_id of policy p2
        attrvalue avalue = new attrvalue();
        for (int jj2 = 0; jj2 < myset1.size(); jj2++) {
            if (!(myset1.get(jj2).equals(Attridcontext))) {
                rp1 = avalue.Values_Ruleset(Attridcontext, Ruleset);
                rp2 = avalue.Values_Ruleset(myset1.get(jj2), Ruleset);
                double[][] matrix1;
                matrix1 = new double[rp1.size()][rp2.size()];
                for (int h2 = 0; h2 < rp1.size(); (h2)++) {
                    for (int h3 = 0; h3 < rp2.size(); h3++) {
                        String s = rp1.get(h2) + "\t" + " " + rp2.get(h3);
                        String sb = rp2.get(h3) + "\t" + " " + rp1.get(h2);
                        Set set2 = hmap.entrySet();
                        Iterator iterator2 = set2.iterator();
                        while (iterator2.hasNext()) {
                            Map.Entry mentry2 = (Map.Entry) iterator2.next();
                            String g = (String) mentry2.getKey();
                            if (g.matches(s) || g.matches(sb)) {
                                double op = asDouble(mentry2.getValue());
                                matrix1[h2][h3] = (op);
                            }
                        }

                    }
                }
                double x1 = 0.0;
                if (rp1.size() >= 1 && rp2.size() >= 1) {
                    x1 = ContingencyTables.symmetricalUncertainty(matrix1);

                }
                if (x1 >= TheresholdNominal) {

                    rd1.add(myset1.get(jj2));
                }

            }//end if()
        }//end for()
        Object[] st2 = rd1.toArray();
        for (Object s : st2) {
            if (rd1.indexOf(s) != rd1.lastIndexOf(s)) {
                rd1.remove(rd1.lastIndexOf(s));
            }
        }
    }

    /**
     * function to calculate hashmap
     * hashmap save pair repeated values
     */
    public HashMap< String, Integer> funct1(ArrayList< String> myset1, ArrayList< Element> Ruleset, HashMap< String, Integer> hmp1) throws SIMException {
        attrvalue attv = new attrvalue();
        for (int j = 0; j < (myset1.size() - 1); j++) { //start of two policies comparison 
            for (int j2 = j + 1; j2 < myset1.size(); j2++) {
                tempCar = new coocure();
                tempoc = new coocure();
                ArrayList< String> carsList00 = new ArrayList< String>(); ////list of attribute value of second attribute comparison(for permit)
                ArrayList< String> carsList01 = new ArrayList< String>();
                ArrayList< String> carsList = new ArrayList< String>();
                ArrayList< String> carsset = new ArrayList< String>();
                for (int tempx = 0; tempx < Ruleset.size(); tempx++) {
                    Node n = Ruleset.get(tempx);
                    if (n.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) n;
                        ArrayList< String> s8 = new ArrayList< String>();
                        s8.addAll(attv.ValueS_RuleElement(myset1.get(j), eElement)[0]);
                        ArrayList< String> s9 = new ArrayList< String>();
                        s9.addAll(attv.ValueS_RuleElement(myset1.get(j2), eElement)[0]);
                        carsList01.addAll(s9);
                        for (int h2 = 0; h2 < s8.size(); (h2)++) {
                            for (int h3 = 0; h3 < s9.size(); h3++) {
                                tempCar.setValue_str1(s8.get(h2));
                                tempCar.setValue_str2(s9.get(h3));
                                carsList.add(tempCar.toString());
                                carsset.add(tempCar.toString());
                                carsList00.add(tempCar.getValue_str2());
                            }
                        }
                    }
                } //end-rules
                Object[] st4 = carsset.toArray();
                for (Object s : st4) {
                    if (carsset.indexOf(s) != carsset.lastIndexOf(s)) {
                        carsset.remove(carsset.lastIndexOf(s));
                    }
                }
                Object[] siu = carsList00.toArray();
                for (Object s : siu) {
                    if (carsList00.indexOf(s) != carsList00.lastIndexOf(s)) {
                        carsList00.remove(carsList00.lastIndexOf(s));
                    }
                }
                for (int e = 0; e < carsset.size(); e++) {
                    int count = 0;
                    for (int w = 0; w < carsList.size(); w++) {
                        if (carsset.get(e).matches(carsList.get(w))) {
                            count = count + 1;
                        }
                    }
                    hmp1.put(carsset.get(e), count);
                }
                for (int t = 0; t < carsList00.size(); t++) {
                    int ci = 0;
                    for (int y = 0; y < carsList01.size(); y++) {
                        if (carsList00.get(t).matches(carsList01.get(y))) {
                            ci = ci + 1;
                        }
                    }
                    hmp1.put(carsList00.get(t), ci);
                }
            }
        }
        return hmp1;
    }
}
